<?php
App::uses('AppController', 'Controller');
/**
 * Saals Controller
 *
 * @property Saal $Saal
 * @property PaginatorComponent $Paginator
 */
class SaalsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

}

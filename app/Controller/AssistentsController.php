<?php
App::uses('AppController', 'Controller');
App::import('Util', 'Util');
/**
 * Assistents Controller
 *
 * @property Assistent $Assistent
 * @property PaginatorComponent $Paginator
 */
class AssistentsController extends AppController {
    var $uses = array('Assistent','Belegung','Saal','Test','Student','Admin');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allow('index');
    }

    public function isAuthorized($user)
    {
        if (in_array($this->request->params['action'], array('view_saal'))) {
            $assistents=array_values($this->Assistent->find('list',array('fields'=>'cipKennung')));
            if(!in_array($user['cipKennung'],$assistents)){
                return false;
            }
        }
        if(in_array($this->request->params['action'], array('admin_add','admin_index'))){
            $admins=array_values($this->Admin->find('list',array('fields'=>'cipKennung')));
            if(!in_array($user['cipKennung'],$admins)){
                return false;  //not admin
            }
        }
        return true;
    }


    public function view_saal() {
        $this->Session->write('Auth.currentAction','Saal Belegung');
        $this->Belegung->recursive =2;
        $this->set('belegungs', $this->Paginator->paginate('Belegung',array('Saal.assistent_id'=>$this->Auth->user('id'))));
    }


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Assistent->recursive = 0;
		$this->set('assistents', $this->Paginator->paginate());
	}


	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Assistent->create();
			if ($this->Assistent->save($this->request->data)) {
				$this->Session->setFlash(__('The assistent has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The assistent could not be saved. Please, try again.'));
			}
		}
	}

}

<?php
App::uses('AppController', 'Controller');
/**
 * Tests Controller
 *
 * @property Test $Test
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TestsController extends AppController {
    var $uses = array('Test','Belegung');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

}

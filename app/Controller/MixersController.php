<?php
App::uses('AppController', 'Controller');
App::import('Util', 'Util');

/**
 * Mixers Controller
 *
 * @property Mixer $Mixer
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class MixersController extends AppController
{
    var $uses = array('Mixer', 'Saal', 'Belegung', 'Einzelanalysis', 'Mischanalysis', 'Test','Admin');

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');


    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('index');
    }

    //describe which action is permitted
    public function isAuthorized($user)
    {
        if (in_array($this->request->params['action'], array('view_saal'))) {
            $mixerLists=array_values($this->Mixer->find('list',array('fields'=>'cipKennung')));
            if(!in_array($user['cipKennung'],$mixerLists)){
                return false;
            }
            $this->Saal->recursive = -1;
            $saals =array_values($this->Saal->find('list', array('conditions' => array('mixer_id' => $user['id']))));
            if(!in_array($this->request->query['saal_name'],$saals)){
                return false;
            }
        }
        if(in_array($this->request->params['action'], array('admin_add','admin_index'))){
            $admins=array_values($this->Admin->find('list',array('fields'=>'cipKennung')));
            if(!in_array($user['cipKennung'],$admins)){
                return false;
            }
        }
        return true;
    }


    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        $this->Session->write('Auth.currentAction', 'Mixer');
        $this->Mixer->recursive = 0;
        $this->set('mixers', $this->Paginator->paginate());
    }


    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add()
    {
        if ($this->request->is('post')) {
            $this->Mixer->create();
            if ($this->Mixer->save($this->request->data)) {
                $this->Session->setFlash(__('The mixer has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The mixer could not be saved. Please, try again.'));
            }
        }
    }

    public function view_tasks()
    {
        //session: cnt_uebung  TODO delete the Auth.cnt_kurss
        $this->set('saalNames', $this->Saal->find('list', array('recursive' => -1, 'conditions' => array('mixer_id' => $this->Auth->user('id')), 'fields' => array('Saal.saal_name'))));
        $this->Session->write('Auth.currentAction', 'Übersicht');

        $saals = $this->Saal->find('list', array('recursive' => -1, 'conditions' => array('mixer_id' => $this->Auth->user('id')), 'fields' => array('Saal.id')));
        $this->Belegung->recursive = 2;
        $this->set('belegungs', $this->Paginator->paginate('Belegung', array('Belegung.saal_id' => $saals)));
    }


    public function view_saal()
    {
        $this->Session->delete('Auth.Belegung');
        if (isset($this->request->query['belegung_id'])) {
            $this->Session->write('Auth.Belegung.id', $this->request->query['belegung_id']);
        }
        $this->set('saalNames', $this->Saal->find('list', array('recursive' => -1, 'conditions' => array('mixer_id' => $this->Auth->user('id')), 'fields' => array('Saal.saal_name'))));
        $this->Session->write('Auth.currentAction', 'Saal ' . $this->request->query['saal_name']);
        $this->Session->write('Auth.Saal.name', $this->request->query['saal_name']);

        //for content
        $this->Belegung->recursive = 2;
        $this->set('belegungs', $this->Paginator->paginate('Belegung', array('mixer_id' => $this->Auth->user('id'), 'Saal.saal_name' => $this->Session->read('Auth.Saal.name'))));

        $alkali = $this->Einzelanalysis->find('list', array('order' => 'name ASC', 'conditions' => array('type' => 'alkali')));
        $this->set('alkali', $alkali);
        $erdalkali = $this->Einzelanalysis->find('list', array('order' => 'name ASC', 'conditions' => array('type' => 'ErdAlkali')));
        $this->set('erdalkali', $erdalkali);
        $nhs = $this->Einzelanalysis->find('list', array('order' => 'name ASC', 'conditions' => array('type' => 'nhs')));
        $this->set('nhs', $nhs);
        $hs = $this->Einzelanalysis->find('list', array('order' => 'name ASC', 'conditions' => array('type' => 'hs')));
        $this->set('hs', $hs);

        $mischanalysesId = $this->Mischanalysis->find('list');
        $this->set('mischanalysesId', $mischanalysesId);
        //todo if the list should be updated
        $mischanalysesList = array();
        $nummer_salzs = $this->Mischanalysis->find('all', array('fields' => array('Mischanalysis.nummer', 'Mischanalysis.salz')));
        foreach ($nummer_salzs as $nummer_salz) {
            $mischanalysesList[$nummer_salz['Mischanalysis']['nummer']] = $nummer_salz['Mischanalysis']['nummer'] . ' ' . $nummer_salz['Mischanalysis']['salz'];
        }
        $this->set('mischanalysesList', $mischanalysesList);

        $mischanalyses = $this->Mischanalysis->find('all');
        $this->set('mischanalyses', $mischanalyses);

        //$this->Test->lockTable('WRITE');
        //----------for saving the data
        if ($this->request->is(array('post', 'put'))) {
          //  $this->Test->unlockTables();
            $expandedBelegungId = str_replace('belegung_', '', $this->request->data['belegung_id']);
            $submitData = $this->request->data;
            array_shift($submitData);

            $this->saveData($expandedBelegungId, $submitData);
        }
        //$this->Test->unlockTables();
    }

    public function saveData($expandedBelegungId = null, $requestData = null)
    {
        $this->autoRender = false;

        $belegung=$this->Belegung->find('first', array('conditions' => array('Belegung.id' => $expandedBelegungId),'recursive' => 0));
        $saal_name=$belegung['Saal']['saal_name'];

        $num = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '_');
        $newRequestArray = array();
        $oldRequestType = '';
        foreach ($requestData as $requestKey => $requestValue) {  //reconstruct the request
            $newRequestType = str_replace($num, '', $requestKey);
            if ($newRequestType == $oldRequestType) {
                $lastGroup = array_pop($newRequestArray);
                $lastGroup[$requestKey] = $requestValue;
                array_push($newRequestArray, $lastGroup);
            } else {
                array_push($newRequestArray, array($requestKey => $requestValue));
            }
            $oldRequestType = $newRequestType;
        }

        foreach ($newRequestArray as $key => $value) {
            $newBody = array();
            $bodyFromSubmit = array();
            $request_type = str_replace($num, '', key($value));
            $test = $this->Test->find('first', array('conditions' =>
                array('Test.analyseType' => $request_type, 'Belegung.id' => $expandedBelegungId)));
            $oldBody = json_decode($test['Test']['body'], true);

            if (!empty(array_filter($value))) {//if not all values are empty
                if(strpos($request_type, 'misch') > 0){//misch analysis
                    debug($value);
                    $inputAnalysis = $this->Mischanalysis->find('first', array('conditions' => array('Mischanalysis.nummer' => current($value))));
                    array_push($bodyFromSubmit, array('mixer' => $inputAnalysis['Mischanalysis'],
                        'student' => '',
                        'correct' => ''));
                }else{
                    foreach ($value as $requestKey => $requestValue) {
                        $request_probe = substr($requestKey, -1);
                        array_push($bodyFromSubmit, array('probe' => $request_probe, 'mixer' => $requestValue, 'student' => '', 'correct' => ''));
                    }
                }

            }

            if ($oldBody['mixerTurn'] != 2) {
                if (!empty($bodyFromSubmit)) {//submitted a input
                    if (!strpos($request_type, 'misch')) { //Einzelanalyse
                        if ($oldBody['mixerTurn'] == 1) {//new submit
                            $newBody['mixerTurn'] = 0;
                            /* debug($oldBody['try']);
                             debug($bodyFromSubmit);*/
                            ensureArray($oldBody['try']);
                            array_push($oldBody['try'], $bodyFromSubmit);
                            $newBody['try'] = $oldBody['try'];
                            //debug($newBody);
                        } elseif ($oldBody['mixerTurn'] == 0) {//changes
                            $newBody['mixerTurn'] = 0;
                            array_pop($oldBody['try']);
                            ensureArray($oldBody['try']);
                            array_push($oldBody['try'], $bodyFromSubmit);
                            $newBody['try'] = $oldBody['try'];
                        }
                    } else {//Misch
                        if ($oldBody['mixerTurn'] == 1) {//new submit
                            $newBody['mixerTurn'] = 0;
                            ensureArray($newBody['try']);
                            array_push($newBody['try'], $bodyFromSubmit);
                        } elseif ($oldBody['mixerTurn'] == 0) {//changes
                            if (empty($oldBody['try'][0][0]['student'])) {//students not edited yet
                                $newBody['mixerTurn'] = 0;
                                array_pop($oldBody['try']);
                                ensureArray($oldBody['try']);
                                array_push($oldBody['try'], $bodyFromSubmit);
                                $newBody['try'] = $oldBody['try'];
                            } else {
                                $newBody['try'] = $oldBody['try'];
                            }
                        }
                    }
                } else {// submitted an empty input
                    if (!strpos($request_type, 'misch')) { //Einzelanalyse
                        if ($oldBody['mixerTurn'] == 1) {//new submit
                            $newBody['mixerTurn'] = 1;
                            if(isset($oldBody['try']))
                                $newBody['try']=$oldBody['try'];
                            //do nothing
                        } elseif ($oldBody['mixerTurn'] == 0) {//changes
                            $newBody['mixerTurn'] = 1;
                            array_pop($oldBody['try']);
                            $newBody['try']=$oldBody['try'];
                        }
                    } else {//Misch
                        if ($oldBody['mixerTurn'] == 1) {//new submit
                            $newBody['mixerTurn'] = 1;
                            //do nothing
                        } elseif ($oldBody['mixerTurn'] == 0) {//changes
                            if (empty($oldBody['try'][0][0]['student'])) {//students not edited yet
                                $newBody['mixerTurn'] = 1;
                            }
                        }
                    }
                }
            } else {  //$oldBody['mixerTurn'] == 2  finished
                $newBody = $oldBody;
            }

            $originalTest = $this->Test->find('first', array('recursive' => -1, 'conditions' => array('id' => $test['Test']['id'])));
            //manipulate the request data
            $this->request->data = $originalTest;
            $this->request->data['Test']['body'] = json_encode($newBody);

            $this->Test->create();
            if (!$this->Test->save($this->request->data)) {
                $this->Session->setFlash('Data save error!');
            }

        }
        $this->redirect(array('controller' => 'mixers', 'action' => 'view_saal', '?' => array('saal_name' => $saal_name, 'belegung_id' => $expandedBelegungId)));
    }

    // Mixer choose Group -> update the anionen and Kationen
    public function inputUpdate()
    {
        if ($this->request->is(array('ajax'))) {
            $this->loadModel('Mischanalysis');

            $mischanalyses = $this->Mischanalysis->find('first', array('conditions' => array('nummer' => $_POST['term'])));

            $allItems = array();
            if (!empty($mischanalyses)) {
                foreach ($mischanalyses['Mischanalysis'] as $key => $value) {
                    array_push($allItems, $value);
                }
            } else {
                array_push($allItems, 'default');
            }

            $this->set(compact('allItems'));
            $this->set('_serialize', array('allItems'));

        }
    }

    public function ionsautocomplete()
    {
        //autocompletion part
        if ($this->request->is(array('ajax'))) {
            $this->loadModel('Einzelanalysis');
            $items = $this->Einzelanalysis->find('list', array(
                'conditions' => array(
                    'name LIKE' => $this->request->query('term') . '%',
                    'type LIKE' => '%' . $this->request->query('gType') . '%'
                )
            ));

            $allItems = array();
            foreach ($items as $key => $value) {
                array_push($allItems, $value);
            }
            //return json_encode($allItems);
            $this->set(compact('allItems'));
            $this->set('_serialize', array('allItems'));
        }
    }


}

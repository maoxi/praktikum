<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
//App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    //an array of the whole row
    public $analyseTypeArray;


    public $components = array(   //introd. 2 Components: session and Auth
        'Session',
        'RequestHandler',
        'Auth' => array(
            /*'loginRedirect' => array(  ////equal to write $this->Auth->loginRedirect=array(xx,xx) TODO should I delete this?
                'controller' => 'students',
                'action' => 'view_scores'
            ),
            /*'logoutRedirect' => array(
                'controller' => 'mainLogins',
                'action' => 'login'
            ),*/
            'authenticate' => array(
                'Student'=>array(
                    'userModel' => 'Student',
                    'fields'=>array(
                        'username'=>'cipKennung',  //TODO last_name also need to be evaluated
                        'password'=>'pwd'
                    ),
                    'passwordHasher' => array(
                        'className'=>'Simple',
                        'hashType'=>'none'
                    )
                ),
                'Mixer'=>array(
                    'userModel' => 'Mixer',
                    'fields'=>array(
                        'username'=>'cipKennung',  //TODO last_name also need to be evaluated
                        'password'=>'pwd'
                    ),
                    'passwordHasher' => array(
                        'className'=>'Simple',
                        'hashType'=>'none'
                    )
                ),
                 'Admin'=>array(
                    'userModel' => 'Admin',
                    'fields'=>array(
                        'username'=>'cipKennung',  //TODO last_name also need to be evaluated
                        'password'=>'pwd'
                    ),
                     'passwordHasher' => array(
                         'className'=>'Simple',
                         'hashType'=>'none'
                     )
                ),
                'Assistent'=>array(
                    'userModel' => 'Assistent',
                    'fields'=>array(
                        'username'=>'cipKennung',
                        'password'=>'pwd'
                    ),
                    'passwordHasher' => array(
                        'className'=>'Simple',
                        'hashType'=>'none'
                    )
                ),
            ),
            'authorize'=>array('Controller'),  //  'authorize'=>array('actionPath'=>'controller') pruefen durch controller
            'authError'=> 'Bitte Kennung Kennwort eingeben'
          )
    );

    public $helpers=array('Js'=>array('Jquery'), 'Session');


    public function isAuthorized($user){ //what a login user have access to
        return true;
    }

    public function beforeFilter(){  //for not login users

        if (!$this->Auth->user() && $this->action != 'login') {
            $this->redirect(array('controller'=>'mainLogins','action'=>'login','admin'=>false));
            //$this->redirect($this->Auth->logoutRedirect);
        }

        $this->Auth->allow('login');  //TODO  allow add method in all model

        $this->analyseTypeArray=array(1=>'alkali',2=>'erdalkali',3=>'nhs',4=>'hs',5=>'gesamt',
            6=>'alkalimisch',7=>'erdalkalimisch',8=>'nhsmisch',9=>'hsmisch',10=>'gesamtmisch');

    }

    public function beforeRender(){
        parent::beforeRender();
    }

}

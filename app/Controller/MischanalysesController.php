<?php
App::uses('AppController', 'Controller');
/**
 * Mischanalyses Controller
 *
 * @property Mischanalysis $Mischanalysis
 * @property PaginatorComponent $Paginator
 */
class MischanalysesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

}

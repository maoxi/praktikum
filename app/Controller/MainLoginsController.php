<?php
App::uses('AppController', 'Controller');
App::uses('ldap', 'Lib');
App::import('Controller','Students');
/**
 * Created by PhpStorm.
 * User: xinyao
 * Date: 04.11.2014
 * Time: 20:53
 * @property SessionComponent $Session
 */

class MainLoginsController extends AppController{
    //var $uses = false;  // do not use any Model or Datenbase
    var $uses = array('Student');

    public function login(){

        if (!empty($this->request->data)) {
            $ldap = new ldap();
            debug($ldap->auth('maoxi', 'novo1987'));
            /*if ($ldap->auth( $this->request->data['User']['cipaccount'],
                $this->request->data['User']['passwd'])) {
                $userRow = $this->User->findByCipaccount($this->request->data['User']['cipaccount']);
                // pr($userRow);
                $user = $userRow['User'];
                $this->Auth->Session->write($this->Auth->sessionKey, $user);
                $this->Auth->_loggedIn = true;
                $this->Session->setFlash('You are logged in!');
                // $this->redirect(array('controller' => 'main', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('Login Failed', true));
            }*/
        }

        if($this->request->is('post')){
            //forward request by faking a request with Student and Mixer
            if($this->request->data('MainLogin.role')==0 ){
                $this->request->data['Student'] = $this->request->data['MainLogin'];
            }else if($this->request->data('MainLogin.role')==1){
                $this->request->data['Mixer'] = $this->request->data['MainLogin'];
            }else if($this->request->data('MainLogin.role')==2){
                $this->request->data['Admin']=$this->request->data['MainLogin'];
            }else{
                $this->request->data['Assistent'] = $this->request->data['MainLogin'];
            }
            //try to login
            debug($this->request->data);
            /*if($this->Auth->login()){
                if($this->request->data('MainLogin.role')==0)
                    $this->redirect(array('controller'=>'students','action'=>'view_scores'));
                else if($this->request->data('MainLogin.role')==1){
                    $this->redirect(array('controller'=>'mixers','action'=>'view_tasks'));
                }else if($this->request->data('MainLogin.role')==2){
                    $this->redirect(array('controller'=>'belegungs', 'action' => 'index', 'admin' => true));
                    //debug($this->Session->check('Auth.User.id')); //if the user is already logged in
                }else{
                    $this->redirect(array('controller'=>'assistents', 'action'=>'view_saal'));
                }
            }else{//fail
                $this->Session->setFlash('your username or password is not correct');
            }*/
        }
    }

    public function logout(){

        $this->Session->destroy();
        $this->redirect($this->Auth->logout());
    }
}
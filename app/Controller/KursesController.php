<?php
App::uses('AppController', 'Controller','Sanitize');
App::import('Util', 'Util');
/**
 * Kurses Controller
 *
 * @property Kurse $Kurse
 * @property PaginatorComponent $Paginator
 */
class KursesController extends AppController {
    var $uses = array('Kurse','Student','Mixer','Belegung','Test','Einzelanalysis','Mischanalysis','Saal','Admin');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

    public function isAuthorized($user)
    {
        if(in_array($this->request->params['action'], array('admin_add'))){
            $admins=array_values($this->Admin->find('list',array('fields'=>'cipKennung')));
            if(!in_array($user['cipKennung'],$admins)){
                return false;  //not admin
            }
        }
        return true;
    }

    //if kurs deleted, then delete all the tests
    public function admin_delete($id = null) {
        $this->Kurse->id = $id;

        $this->Belegung->updateAll(
            array('Belegung.punkt' => 0,'note' => "'n.b.'"),
            array('Belegung.kurse_id ' => $id)
        );

        //update the schwerigkeit record in einzelanalyse
        $this->Einzelanalysis->updateAll(
            array('oldOccurence'=>'oldOccurence+occurence','oldError'=>'oldError+error','occurence'=>0,'error'=>0)
        );

        if ($this->Kurse->delete() && $this->Test->query('TRUNCATE table tests;') ) {
            $this->Session->setFlash(__('The kurse has been deleted.'));
        } else {
            $this->Session->setFlash(__('The kurse could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'admin_add'));
    }


	public function admin_add() {
        $this->Session->write('Auth.currentAction','Neuen Kurs starten');

        $this->Kurse->recursive = 0;
        $this->set('kurses', $this->Paginator->paginate());

		if ($this->request->is('post')) {
                $this->Kurse->create();
                //create tests
                $blgs=$this->Belegung->find('all');
                foreach($blgs as $blg){
                    for($i=1; $i<=count($this->analyseTypeArray);$i++){
                        $en_analyse_body=$this->analyseobj_json();
                        $this->Test->create();
                        $this->Test->save(array('analyseType'=>$this->analyseTypeArray[$i],'belegung_id'=>$blg['Belegung']['id'],'body'=>$en_analyse_body));
                    }
                }

                if ($this->Kurse->save($this->request->data)) {
                    $this->Belegung->updateAll(
                        array('Belegung.kurse_id' => $this->Kurse->id)
                    );
                    $this->Session->setFlash(__('The kurse has been saved.'));
                    return $this->redirect(array('action' => 'admin_add'));
                } else {
                    $this->Session->setFlash(__('The kurse could not be saved. Please, try again.'));
                }
		}
	}

    /**
     * create the json string
     * used in function admin_add()
     */
    public function analyseobj_json(){
        $this->autoRender=true; //dont render a view
        //$this->request->allowMethod('ajax'); //No direct access via browser URL
        //create an json body
        $analyseBody=array(
            'mixerTurn'=>1,
        );
        return json_encode($analyseBody);
    }
}


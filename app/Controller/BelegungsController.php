<?php
App::uses('AppController', 'Controller');
/**
 * Belegungs Controller
 *
 * @property Belegung $Belegung
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class BelegungsController extends AppController {
    var $uses = array('Belegung','Saal','Assistent','Mixer','Admin');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');


    public function isAuthorized($user)
    {
        if(in_array($this->request->params['action'], array('admin_edit_mixer','admin_edit_student','admin_index'))){
            $admins=array_values($this->Admin->find('list',array('fields'=>'cipKennung')));
            if(!in_array($user['cipKennung'],$admins)){
                return false;  //not admin
            }
        }
        return true;
    }

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Belegung->recursive = 2;
		$this->set('belegungs', $this->Paginator->paginate());
        $this->Session->write('Auth.currentAction','Saal Belegung');
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit_student($id = null) {
        $this->set('students', $this->Belegung->Student->find('list'));  //ganz spalte, benutzt zusammen mit XXX_id
        $this->set('kurses',$this->Belegung->Kurse->find('list'));
        $this->set('saals',$this->Saal->find('list'));

		if (!$this->Belegung->exists($id)) {
			throw new NotFoundException(__('Invalid belegung'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Belegung->save($this->request->data)) {
				$this->Session->setFlash(__('The belegung has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The belegung could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Belegung.' . $this->Belegung->primaryKey => $id));
			$this->request->data = $this->Belegung->find('first', $options);
        }
	}

    public function admin_edit_mixer($id = null) {
        $this->set('mixers',$this->Mixer->find('list'));
        $this->set('saals',$this->Saal->find('list'));
        $this->set('saal',$this->Saal->find('first',array('conditions'=>array('Saal.id'=>$id))));

        if (!$this->Saal->exists($id)) {
            throw new NotFoundException(__('Invalid belegung'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Saal->save($this->request->data)) {
                $this->Session->setFlash(__('The belegung has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The belegung could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Saal.' . $this->Saal->primaryKey => $id));
            $this->request->data = $this->Saal->find('first', $options);
        }
    }

}

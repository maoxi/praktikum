<?php
App::uses('AppController', 'Controller');
App::import('Util', 'Util');
/**
 * Students Controller
 *
 * @property Student $Student
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class StudentsController extends AppController {
    var $uses = array('Kurse','Student','Belegung','Test','Einzelanalysis','Mischanalysis','Admin','Saal');

	public $components = array('Paginator', 'Session');


    /*    public function beforeFilter(){
         parent::beforeFilter();
         $this->Auth->allow('index');
     }

     //describe which action is permitted
  public function isAuthorized($user)
     {
         if (in_array($this->request->params['action'], array('view_kurse','view_scores'))) {
             $students=array_values($this->Student->find('list',array('fields'=>'lastFirst_name')));
             if(!in_array($user['lastFirst_name'],$students)){
                 return false;
             }
         }
         if(in_array($this->request->params['action'], array('admin_save_excels','admin_add','admin_index'))){
             $admins=array_values($this->Admin->find('list',array('fields'=>'lastFirst_name')));
             if(!in_array($user['lastFirst_name'],$admins)){
                 return false;
             }
         }
         return true;
     }*/

    public function view_scores() {
        //for action
        $this->Session->write('Auth.cnt_kurses',$this->Student->Belegung->find('count',array('conditions'=>array('student_id'=>$this->Auth->user('id')),'group'=>'kurse_id')));
        $this->Session->write('Auth.currentAction','Punkteübersicht');

        //for content
        $this->Belegung->recursive =2;
        $this->set('belegungs',$this->Paginator->paginate('Belegung',array('student_id'=>$this->Auth->user('id'))));
    }

    public function view_kurse(){
        $this->Session->delete('Auth.Test');
        if(isset($this->request->query['test_id'])){
            $this->Session->write('Auth.Test.id',$this->request->query['test_id']);
        }
        $this->Session->write('Auth.currentAction','Analysen');
        //for content
        $this->Belegung->recursive =2;
        $this->set('belegungs', $this->Paginator->paginate('Belegung',array('student_id'=>$this->Auth->user('id'))));
        //autocompletion
        $alkali = $this->Einzelanalysis->find('list', array('order' => 'name ASC', 'conditions' => array('type' => 'alkali')));
        $this->set('alkali', $alkali);
        $erdalkali = $this->Einzelanalysis->find('list', array('order' => 'name ASC', 'conditions' => array('type' => 'erdalkali')));
        $this->set('erdalkali', $erdalkali);
        $nhs = $this->Einzelanalysis->find('list', array('order' => 'name ASC', 'conditions' => array('type' => 'nhs')));
        $this->set('nhs', $nhs);
        $hs = $this->Einzelanalysis->find('list', array('order' => 'name ASC', 'conditions' => array('type' => 'hs')));
        $this->set('hs', $hs);

        if ($this->request->is(array('post', 'put'))) {
            $session_test=$this->Test->find('first', array('recursive'=> -1,'conditions'=> array('id' => $this->request->data['test_id'])));
            $this->Session->write('Auth.Test',$session_test['Test']);
            $kurse_id=$this->request->data['kurse_id'];

            $this->save_input($session_test,$kurse_id);
        }
    }


    public function save_input($session_test=null,$kurse_id=null){
        $this->autoRender=false;
        //recollect the input from student and save as a string in body
        $testBody=json_decode($session_test['Test']['body'],true);
        $cntTry = count($testBody)==1? 0:count($testBody['try']);
        $scoreUpdated=false;
        $punkt=0;

        if(strpos($session_test['Test']['analyseType'],'misch')){
            $stdAnionInputString=$this->request->data['anionen'];
            $stdKationInputString=$this->request->data['kationen'];
            $stdAnionInput=array_filter(preg_split('/[;,. ]/',strtolower($stdAnionInputString)));
            $stdKationInput=array_filter(preg_split('/[;,. ]/',strtolower($stdKationInputString)));

            $requestBodyMixer=$testBody['try'][$cntTry-1][0]['mixer'];
            $mixerAnionInput=array_filter(preg_split('/[;,. ]/',strtolower($requestBodyMixer['anionen'])));
            $mixerKationInput=array_filter(preg_split('/[;,. ]/',strtolower($requestBodyMixer['kationen'])));
            $cntAnionIntersect=array_intersect($stdAnionInput,$mixerAnionInput);
            $cntKationIntersect=array_intersect($stdKationInput,$mixerKationInput);
            $aZuwenig=count($mixerAnionInput)-count($cntAnionIntersect);
            $aZuviel=count($stdAnionInput)-count($cntAnionIntersect);
            $kZuwenig=count($mixerKationInput)-count($cntKationIntersect);
            $kZuviel=count($stdKationInput)-count($cntKationIntersect);
            $zuwenig= $aZuwenig+ $kZuwenig;
            $zuviel= $aZuviel+ $kZuviel;
            $mistakes=0;

            $testBody['try'][$cntTry-1][0]['student']=array('kationen'=>$stdKationInputString,'anionen'=>$stdAnionInputString);
            if($zuwenig==0 && $zuviel==0){
                $testBody['mixerTurn']=0;
                $testBody['try'][$cntTry-1][0]['correct']=array('correct'=>'true','aZuwenig'=>0,'aZuviel'=>0,'kZuwenig'=>0,'kZuviel'=>0);
            }else{
                $testBody['mixerTurn']=0;
                $testBody['try'][$cntTry-1][0]['correct']=
                    array('correct'=>'false','aZuwenig'=>$aZuwenig,'aZuviel'=>$aZuviel,'kZuwenig'=>$kZuwenig,'kZuviel'=>$kZuviel);
                //create another chance for students if false
                if($cntTry<2){
                    $testBody['try'][$cntTry][0]['mixer']=$testBody['try'][$cntTry-1][0]['mixer'];
                    $testBody['try'][$cntTry][0]['student']='';
                    $testBody['try'][$cntTry][0]['correct']='';
                }
            }
            if(($zuwenig==0 && $zuviel==0)|| $cntTry==2){ //test is finished, calculate the punkt
                $testBody['mixerTurn']=2;
                foreach($testBody['try'] as $try){
                    $mistakes=$mistakes+$try[0]['correct']['aZuwenig']+$try[0]['correct']['aZuviel']
                        +$try[0]['correct']['kZuwenig']+$try[0]['correct']['kZuviel'];
                }
                $punkt=max(2+(6-$mistakes)*3,0);
                $scoreUpdated=true;
            }
        }else{//einzel analyse
            $cntFalse=0;
            $testBody['mixerTurn']=1;
            for($j=0;$j<count($testBody['try'][$cntTry-1]);$j++){
                $testBody['try'][$cntTry-1][$j]['student']=$this->request->data[$j]; //only edit the last try
                //correct the answer
                if(trim(strtolower($testBody['try'][$cntTry-1][$j]['student']))==trim(strtolower($testBody['try'][$cntTry-1][$j]['mixer']))){
                    $testBody['try'][$cntTry-1][$j]['correct']='true';
                    $this->Einzelanalysis->updateAll(
                        array('occurence'=>'occurence+1'),
                        array('name'=>strtoupper($testBody['try'][$cntTry-1][$j]['mixer']),'type'=>$session_test['Test']['analyseType'])
                        );
                }else{
                    $testBody['try'][$cntTry-1][$j]['correct']='false';
                    $cntFalse++;
                    //record the error rate
                    $this->Einzelanalysis->updateAll(
                        array('occurence'=>'occurence+1','error'=>'error+1'),
                        array('name'=>strtoupper($testBody['try'][$cntTry-1][$j]['mixer']),'type'=>$session_test['Test']['analyseType'])
                    );
                }
            }
            if($cntTry>0){ //this group of tests is finished
                //calculate the punkt
                for($j=0;$j<count($testBody['try'][0]);$j++){
                    if($testBody['try'][0][$j]['correct']==='true'){
                        $punkt+=4;
                    }
                }
                $scoreUpdated=true;
            }
            if($cntFalse==0 && $cntTry>0){
                $testBody['mixerTurn']=2;
            }
        }

        //manipulate the request data
        $this->request->data=$session_test;
        $this->request->data['Test']['body']=json_encode($testBody);
        $this->request->data['Test']['punkt']=$punkt;

        $this->Test->create();
        if($this->Test->save($this->request->data)){
            if($scoreUpdated){
                $this->update_score($this->Session->read('Auth.Test.belegung_id'));
            }
            $this->Session->setFlash(__('The test has been saved.'));
            $this->redirect(array('controller'=>'students','action' => 'view_kurse','?' => array('kurse_id'=>$kurse_id,'test_id'=>$session_test['Test']['id'])));
        }else {
            $this->Session->setFlash(__('The test could not be saved. Please, try again.'));
        }
    }

    public function update_score($belegung_id){
        $this->autoRender=false;
        $belegungs = $this->Belegung->find('first', array('conditions' => array('Belegung.id' => $belegung_id)));
        $totalPunkt=0;
        $totalNote=0;
        foreach($belegungs['Test'] as $eachTest){
            isset($eachTest['punkt'])? $totalPunkt+=$eachTest['punkt']: $totalPunkt+=0;
        }

        if($totalPunkt >=142){
            $totalNote='1.0';
        }elseif($totalPunkt>= 136){
            $totalNote='1.3';
        }elseif($totalPunkt>=129){
            $totalNote='1.7';
        }elseif($totalPunkt>=122){
            $totalNote='2.0';
        }elseif($totalPunkt>=115){
            $totalNote='2.3';
        }elseif($totalPunkt>=108){
            $totalNote='2.7';
        }elseif($totalPunkt>=101){
            $totalNote='3.0';
        }elseif($totalPunkt>=94){
            $totalNote='3.3';
        }elseif($totalPunkt>=87){
            $totalNote='3.7';
        }elseif($totalPunkt>=80){
            $totalNote='4.0';
        }else{
            $totalNote='n.b.';
        }
        $this->Belegung->id=$belegungs['Belegung']['id'];
        $this->Belegung->saveField('punkt',$totalPunkt);
        $this->Belegung->saveField('note',$totalNote);
    }

    //for auto completion
    public function ionsautocomplete(){
        if ($this->request->is(array('ajax'))) {
            $this->loadModel('Einzelanalysis');
            $items = $this->Einzelanalysis->find('list', array(
                'conditions' => array(
                    'name LIKE' => '%'.$this->request->query('term').'%',  //TODO conditon 'type LIKE'=>
                )
            ));

            $allItems = array();
            foreach ($items as $key => $value){
                array_push($allItems, $value);
            }

            //return json_encode($allItems);
            $this->set(compact('allItems'));
            $this->set('_serialize', array('allItems'));
        }
    }

    public function admin_save_excels(){
        //$this->layout='ajax';
        //TODO  paginate?
        $this->loadModel('Belegung');
        $belegungs=$this->Belegung->find('all',array(
            'fields'=>array('Belegung.punkt','Belegung.note','Belegung.kurse_id','Student.first_name','Student.last_name','Student.martikelNr')
        ));
        $this->set('belegungs',$belegungs);
    }

    public function admin_view_statistik(){
        //for action
        $this->Session->write('Auth.currentAction','Statistiken');
        //for content
        $this->set('saals',$this->Saal->find('all'));

        //for every Saal
        $saalPunkte=array();
        for($i=1;$i<7;$i++){
            $this->Belegung->recursive=2;
            $saalBelegung=$this->Belegung->find('all',array('conditions'=>array('saal_id'=> $i)));
            $saalPunkte[]=$this->reloadData($saalBelegung);
        }
        $this->set('saalPunkte',$saalPunkte);
        //zusammen
        $this->Belegung->recursive =2;
        $belegungs=$this->Belegung->find('all');
        $this->set('belegungs',$belegungs);
        $this->set('zusammen',$this->reloadData($belegungs));
        //note
        $this->Belegung->recursive=-1;
        $noteBelegungs=$this->Belegung->find('all',array('fields'=>array('Belegung.note','COUNT(Belegung.id) AS total_number'),'group'=>'note'));
        $noteVerteilung=array();
        foreach($noteBelegungs as $noteBelegung){
            $noteVerteilung[]=array('note'=>$noteBelegung['Belegung']['note'],'number'=>$noteBelegung[0]['total_number']);
        }
        $this->set('noteVerteilung',$noteVerteilung);
        //schwerigkeit
        $einzelanalysis=$this->Einzelanalysis->find('all',array('fields'=>
            array('name',
                'SUM(Einzelanalysis.oldOccurence) AS oocc',
                'SUM(Einzelanalysis.oldError) AS oerr',
                'SUM(Einzelanalysis.occurence) AS occ',
                'SUM(Einzelanalysis.error) AS err'),
            'group'=>'name'));
        $schwerigkeit=array();
        foreach($einzelanalysis as $einzelanalyse){
            $entry=array();
            $entry['name']=$einzelanalyse['Einzelanalysis']['name'];
            $entry['wrong']=$einzelanalyse[0]['oerr']+$einzelanalyse[0]['err'];
            $entry['right']=$einzelanalyse[0]['oocc']+$einzelanalyse[0]['occ']-$entry['wrong'];
            $schwerigkeit[]=$entry;
        }
        $this->set('schwerigkeit',$schwerigkeit);
    }

    public function reloadData($data){
        $returnData = array();
        foreach($data as $datum){
            $entry=array();
            $entry['name'] = $datum['Student']['first_name'].' '.$datum['Student']['last_name'];
            foreach($datum['Test'] as $test){
                $entry[rewriteType($test['analyseType'])]=$test['punkt'];
            }
            $returnData[]=$entry;
        }
        return $returnData;
    }

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Student->create();
			if ($this->Student->save($this->request->data)) {
				$this->Session->setFlash(__('The student has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The student could not be saved. Please, try again.'));
			}
		}
	}

    public function admin_index() {
        $this->Session->delete('Auth.Student');
        if(isset($this->request->query['student_id'])){
            $this->Session->write('Auth.Student.id',$this->request->query['student_id']);
        }
        $this->Session->write('Auth.currentAction','Student & Note');

        //for content
        $this->Belegung->recursive =2;
        $this->set('belegungs', $this->Paginator->paginate('Belegung'));
    }
}

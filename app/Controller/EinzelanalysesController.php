<?php
App::uses('AppController', 'Controller');
/**
 * Einzelanalyses Controller
 *
 * @property Einzelanalysis $Einzelanalysis
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IonsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

}

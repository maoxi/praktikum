<?php
App::uses('AppModel', 'Model');
/**
 * Test Model
 *
 * @property Belegung $Belegung
 */
class Test extends AppModel {
    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array('Belegung');


/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'analyseType';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'analyseType' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'body' => array(
			//'notEmpty' => array(
				//'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			//),
		),
		'belegung_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
        'punkt' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
	);

  /*  function lockTableRW() {
        $this->lockTable('READ');
        $this->lockTable('WRITE');
    }*/

    function lockTable($type='READ') {
        $dbo = $this->getDataSource();
        $dbo->execute('LOCK TABLES '.$this->table.' '.$type);
    }

    function unlockTables() {
        $dbo = $this->getDataSource();
        $dbo->execute('UNLOCK TABLES');
    }


}

<?php
App::uses('AppModel', 'Model');
/**
 * Einzelanalysis Model
 *
 */
class Einzelanalysis extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'name';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}

<?php
App::uses('AppModel', 'Model');
/**
 * Mischanalysis Model
 *
 */
class Mischanalysis extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'nummer';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nummer';

}

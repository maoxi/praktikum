<?php
App::uses('AppModel', 'Model');
/**
 * Saal Model
 *
 * @property Mixer $Mixer
 * @property Belegung $Belegung
 */
class Saal extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'id';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'saal_name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
         'Mixer','Assistent'
        /* 'Mixer' => array(
             'className' => 'Mixer',
             'foreignKey' => 'mixer_id',
             'conditions' => '',
             'fields' => '',
             'order' => ''
         ),
         'Assistent' => array(
             'className' => 'Mixer',
             'foreignKey' => 'mixer_id',
             'conditions' => '',
             'fields' => '',
             'order' => ''
         )*/
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Belegung' => array(
			'className' => 'Belegung',
			'foreignKey' => 'saal_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}

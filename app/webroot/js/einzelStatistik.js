/**
 * Created by xinyao on 11.02.2015.
 */

function drawEinzelStatistik(dataArray,slt){
    var margin = {top: 20, right: 20, bottom: 30, left: 40},
        width = 960 - margin.left - margin.right,
        height = 550 - margin.top - margin.bottom;

    var x0 = d3.scale.ordinal()
        .rangeRoundBands([0, width], .1);   //width-100

    var x1 = d3.scale.ordinal();

    var y = d3.scale.linear()
        .range([height, 0]);

    var color = d3.scale.ordinal()
        .range(["#9ACD32", "#6B8E23", "#808000", "#556B2F", "#8FBC8F", "#66CDAA", "#20B2AA", "#008B8B", "#2F4F4F", "#000000"]);

    var xAxis = d3.svg.axis()
        .scale(x0)   //x
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .ticks(8)
        .tickFormat(d3.format(".2d"));

    var svg = d3.select(slt).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var groupNames = d3.keys(dataArray[0]).filter(function (key) {
        return key !== "name";
    });

    dataArray.forEach(function (d) {
        d.groups = groupNames.map(function (name) {
            return {name: name, value: d[name]};
        });
    });

    x0.domain(dataArray.map(function (d) {
        return d.name;
    }));
    x1.domain(groupNames).rangeRoundBands([0, x0.rangeBand()]);
    y.domain([0,20]);

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Punkte");

    var name = svg.selectAll(".name")
        .data(dataArray)
        .enter().append("g")
        .attr("class", "g")
        .attr("transform", function (d) {
            return "translate(" + x0(d.name) + ",0)";
        });

    svg.selectAll(".g")
        .selectAll("rect")
        .data(function (d) {
            return d.groups;
        })
        .enter().append("rect")
        .attr("width", x1.rangeBand())
        .attr("x", function (d) {
            return x1(d.name);
        })
        .attr("y", function (d) {
            return y(d.value);
        })
        .attr("height", function (d) {
            return height - y(d.value);
        })
        .style("fill", function (d) {
            return color(d.name);
        })
        .attr("id", function(d,i) {
            return "rect-" + i;
        })
        .on("mouseover", function(d,i) {
            svg.selectAll("#rect-" + i).style("fill", "red");
        })
        .on("mouseout", function(d,i) {
            svg.selectAll("#rect-" + i).style("fill", function (d) {
                return color(d.name);
            });
        });

    var legend = svg.selectAll(".legend")
        .data(groupNames.slice())
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function (d, i) {
            return "translate(0," + i * 20 + ")";
        });

    legend.append("rect")
        .attr("x", width)
        .attr("width", 8)
        .attr("height", 18)
        .style("fill", color)
        .on("mouseover", function(d,i) {
            svg.selectAll("#rect-" + i).style("fill", "red");
        })
        .on("mouseout", function(d,i) {
            svg.selectAll("#rect-" + i).style("fill", function (d) {
                return color(d.name);
            });
        });


    legend.append("text")
        .attr("x", width-2 )
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function (d) {
            return d;
        });

}
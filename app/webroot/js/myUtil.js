/**
 * Created by xinyao on 08.01.2015.
 */
var analyseTypeArray=new Array('erdalkali','alkali','nhs','hs');  //,'Zusammen'

//student_edit_test.ctp
function enableSaveBtn(){
    var $input = $(this);
    var className=$input.attr('class').split(' ')[0];
    setTimeout(function() {
        if($input.val().length > 0){
            $('.btn_std_save_test.'+className).show();
        }else{
            $('.btn_std_save_test.'+className).hide();
        }
    },0);
}

function enableEditBtn(event){
    event.preventDefault();
    var input=$(this);
    var className=input.attr('class').split(' ')[0];
    if(input.text()==='Save'){  //save mode
        $('input.'+className).prop('disabled',true);
        input.text('Edit');
        input.next('button').show();
    }else{   //edit mode
        $('input.'+className).prop('disabled',false);
        input.text('Save');
        input.next('button').hide();
    }
}

//student_edit_test.ctp
function saveResult(){
    event.preventDefault();
    var input=$(this);
    var className=input.attr('class').split(' ')[0];
    var std_confirm=confirm('Attention: Results cannot be changed if submitted!');
    if(std_confirm==true){
        //$(this).parents('div.level3').children('.autoComplete').prop('disabled',false); //enable again
        $('input.'+className).prop('disabled',false);
        $('form#'+className).submit();
    }
}

function saveMixerResult(){
    event.preventDefault();
    var input=$(this);
    var className=input.attr('class').split(' ')[0];
    //enable the dropdownlist
    $('select').prop('disabled',false);
    $('form#'+className).submit();
}


//edit_test.ctp
//mistakes print in red
/*$("table td").each(function (){
    var input=$(this);
    if(input.html().indexOf('falsch')>-1){
        input.css({"color":"#aa0000","font-weight":"bold"});
    }
});*/

//view_uebung.ctp
//show the ions table
function showIonTable(){
    for(var i=0; i<analyseTypeArray.length;i++) {  // first hide all the table shown
        $('div.'.concat(analyseTypeArray[i])).css('bottom', '100%');
    }
    if(!$(this).prop('readonly') && !$(this).prop('disabled') ){
        $focused=$(this);
        //choose the group to show
        for(var i=0; i<analyseTypeArray.length; i++){
            if($focused.attr('class').split(' ')[0].indexOf(analyseTypeArray[i])>-1){
                $('div.'.concat(analyseTypeArray[i])).css('bottom','0%');
                $('body').animate({scrollTop: $focused.position().top-100},'500');
            }
        }
    }
}
//view_uebung.ctp
//click elsewhere, the ions table disapppear
function hideIonTable(){
    if(event.target.nodeName!='INPUT' && event.target.nodeName!='BUTTON'){
        for(var i=0; i<analyseTypeArray.length;i++) {
            $('div.'.concat(analyseTypeArray[i])).css('bottom', '100%');
        }
    }
}

//view_uebung.ctp
//myButtons (css= certain width)
$('div.myButtons button').each(function(){
    var input=$(this);
    if(input.width()<110){
        input.css('width','110px')
    }else{
        input.css('width','220px');
    }
});
//view_uebung.ctp
//myButtons click handler
function changeTableFocus(){
    event.preventDefault();
    var input=$(this);
    $focused.val(input.html());
    var inputs = $focused.closest('tbody').find('input');
    inputs.eq( inputs.index($focused)+ 1 ).focus();
}
//view_uebung.ctp
//edit the filled blanks
function deleteEinzelInput(){
    $(this).prev('div').children('input').prop('readonly',false);
    $(this).prop('hidden',true);
    $(this).prev('div').children('input').prop('value',null);
}
function deleteMischInput(){
    $(this).siblings('div.select').children('select').prop('disabled',false);
    $(this).prop('hidden',true);

}

/*//view_uebung.ctp
//mistakes print in red
    $("table table td").each(function (){
        var input=$(this);
        if(input.html()=='falsch'){
            input.css({"color":"#aa0000","font-weight":"bold"});
        }
    });*/


//view_saal.ctp
//click level2=>show level3
function level2(){
    var input=$(this);
    if(event.target.className==="showlevel3"){  //show the current level3 div
        var selected=event.target.id;
        if(event.target.nodeName=='A'){
            $('.current').removeClass('current');
            $(event.target).closest('tr').addClass('current');
        }
        if(event.target.nodeName=='DIV'){
            $('.current').removeClass('current');
            $(event.target).closest('a').addClass('current');
        }

        $('div.level1').hide();
        $('div.level2').animate({'width':'30%'});
        //hide all the others
        $('div.level3').hide();
        $('div.level3').find('input').prop('disabled',true);
        setTimeout(
            function() {
                $('#level3'+selected).animate({opacity:"show"});
                $('#level3'+selected).find('input').prop('disabled',false);
                $('input.kationen, input.anionen').prop('disabled',true);
            }, 120);
       /* $('div.level3#'+selected).show();*/
    }else{
        if(input.css('width')!='55%'){   //show only till level2
            $('div.level3').hide();
            $('div.level1').show();
            $('div.level2').animate({'width':'75%' })
            $('.current').removeClass('current');
        }
    }
}

//view_saal.ctp
function level2Mouseenter(){
    if($('div.level2').css('zoom')===0.5){
        $(this).animate({'zoom':'0.55'});
    }
}
//view_saal.ctp
function level2Mouseleave(){
    if($('div.level2').css('zoom')===0.55){
        $(this).animate({'zoom':'0.5'});
    }
}
/**
 * Created by xinyao on 11.02.2015.
 */

function drawStackedStatistikY(dataArray,slt){
    var margin = {top: 40, right: 50, bottom: 30, left: 150},
        width = 960 - margin.left - margin.right,
        height = 6000 - margin.top - margin.bottom;

    var y = d3.scale.ordinal()
        .rangeRoundBands([0, height], .1);   //width-100

    var x = d3.scale.linear()
        .range([0,width]);

    var color = d3.scale.ordinal()
        .range(["#aa0000","#9ACD32"]);

    var xAxis = d3.svg.axis()
        .scale(x)   //x
        .orient("top")
        .tickFormat(d3.format(".2d"));

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

    var svg = d3.select(slt).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    color.domain(d3.keys(dataArray[0]).filter(function(key) { return key !== "name"; }));

    dataArray.forEach(function(d) {
        var x0 = 0;
        d.groups = color.domain().map(function(name) { return {name: name, x0: x0, x1: x0 += +d[name] }; });
        d.total = d.groups[d.groups.length-1].x1;
    });

    dataArray.sort(function(a, b) { return b.total - a.total; });

    y.domain(dataArray.map(function(d) { return d.name; }));
    x.domain([0, d3.max(dataArray, function(d) { return d.total; })]);
    //y.domain([0, 160]);

    svg.append("g")
        .attr("class", "x axis")
        //.attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    /*.text("Punkte");*/

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("y", 6)  //todo change relative pos
        .attr("dy", ".71em")
        .style("text-anchor", "end");

    var name = svg.selectAll(".name")
        .data(dataArray)
        .enter().append("g")
        .attr("class", "g")
        .attr("transform", function(d) { return "translate(0," + y(d.name) + ")"; });

    svg.selectAll('.g')
        .selectAll("rect")
        .data(function(d) { return d.groups; })
        .enter().append("rect")
        //.attr("y", function(d) { return y(d.name); })
        .attr("height",y.rangeBand())
        .attr("x", function(d) { return x(d.x0); })
        .attr("width", function(d) { return x(d.x1)-x(d.x0); })
        .style("fill", function(d) { return color(d.name); });


    var legend = svg.selectAll(".legend")
        .data(color.domain().slice())
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });


    legend.append("rect")
        .attr("x", width)
        .attr("width", 8)
        .attr("height", 18)
        .style("fill", color);


    legend.append("text")
        .attr("x", width-2 )
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function (d) {
            return d;
        });


    $(':checkbox').change(function(){
            var y0 = y.domain(dataArray.sort(this.checked
                ? function(a, b) {  return b.wrong/(b.right+b.wrong+1) - a.wrong/(a.right+a.wrong+1); }
                : function(a, b) {  return (b.right+ b.wrong)-(a.right+ a.wrong); })
                .map(function(d) { return d.name; }))
                .copy();

            var transition = svg.transition().duration(750),
                delay = function(d, i) { return i * 5; };

            transition.selectAll("g.g")
                .delay(delay)
                .attr("transform", function(d) { return  "translate(0," + y0(d.name) + ")"; });

            transition.select(".y.axis")
                .call(yAxis)
                .selectAll("g")
                .delay(delay);
        });




}

/**
 * Created by xinyao on 11.02.2015.
 */

function drawNoteStatistik(noteVerteilung,slt){
    var margin = {top: 20, right: 20, bottom: 30, left: 40},
        width = 960 - margin.left - margin.right,
        height = 550 - margin.top - margin.bottom;

    var x0 = d3.scale.ordinal()
        .rangeRoundBands([0, width], .1);   //width-100

    var x1 = d3.scale.ordinal();

    var y = d3.scale.linear()
        .range([height, 0]);

    var color = d3.scale.ordinal()
        .range(["#9ACD32", "#6B8E23", "#808000", "#556B2F", "#8FBC8F", "#66CDAA", "#20B2AA", "#008B8B", "#2F4F4F", "#000000"]);

    var xAxis = d3.svg.axis()
        .scale(x0)   //x
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickFormat(d3.format("d"))
        .tickSubdivide(0);

    var svg = d3.select(slt).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    x0.domain(noteVerteilung.map(function(d) { return d.note; }));
    y.domain([0,d3.max(noteVerteilung,function(d){return d.number})]);
    //d3.extent(noteVerteilung, function(d) { return d.number; }));

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Anzahl");

    var line = d3.svg.line()
        //.interpolate('basis')
        .x(function(d) { return x0(d.note)+70; })
        .y(function(d) { return y(d.number); });

    svg.selectAll(".line")
        .data([noteVerteilung])
        .enter().append("path")
        .attr("class", "line")
        .attr("d", line);

    /*function make_x_axis() {
     return d3.svg.axis()
     .scale(x0)
     .orient("bottom")
     .ticks(5)
     }*/

    function make_y_axis() {
        return d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(5)
    }

    svg.append("g")
        .attr("class", "grid")
        .call(make_y_axis()
            .tickSize(-width, 0, 0)
            .tickFormat("")
    )
}
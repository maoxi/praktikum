/**
 * Created by xinyao on 11.02.2015.
 */

function drawStackedStatistik(dataArray,slt){
    var margin = {top: 20, right: 20, bottom: 30, left: 40},
        width = 960 - margin.left - margin.right,
        height = 550 - margin.top - margin.bottom;

    var x = d3.scale.ordinal()
        .rangeRoundBands([0, width], .1);   //width-100

    var y = d3.scale.linear()
        .range([height, 0]);

    var color = d3.scale.ordinal()
        .range(["#9ACD32", "#6B8E23", "#808000", "#556B2F", "#8FBC8F", "#66CDAA", "#20B2AA", "#008B8B", "#2F4F4F", "#000000"]);

    var xAxis = d3.svg.axis()
        .scale(x)   //x
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickFormat(d3.format(".2d"));

    var svg = d3.select(slt).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    color.domain(d3.keys(dataArray[0]).filter(function(key) { return key !== "name"; }));

    dataArray.forEach(function(d) {
        var y0 = 0;
        d.groups = color.domain().map(function(name) { return {name: name, y0: y0, y1: y0 += +d[name] }; });
        d.groups.pop();
        d.total = d.groups[d.groups.length-1].y1;
    });

    dataArray.sort(function(a, b) { return b.total - a.total; });

    x.domain(dataArray.map(function(d) { return d.name; }));
    y.domain([0, d3.max(dataArray, function(d) { return d.total; })]);
    //y.domain([0, 160]);

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")

        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
    /*.text("Punkte");*/

    var name = svg.selectAll(".name")
        .data(dataArray)
        .enter().append("g")
        .attr("class", "g")
        .attr("transform", function(d) { return "translate(" + x(d.name) + ",0)"; });

    svg.selectAll('.g')
        .selectAll("rect")
        .data(function(d) { return d.groups; })
        .enter().append("rect")
        .attr("width", x.rangeBand())
        .attr("y", function(d) { return y(d.y1); })
        .attr("height", function(d) { return y(d.y0) - y(d.y1); })
        .style("fill", function(d) { return color(d.name); });

 /*   var legend = svg.selectAll(".legend")
        .data(color.domain().slice())
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

    legend.append("rect")
        .attr("x", width)
        .attr("width", 8)
        .attr("height", 18)
        .style("fill", color);

    legend.append("text")
        .attr("x", width-2 )
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function (d) {
            return d;
        });*/
}

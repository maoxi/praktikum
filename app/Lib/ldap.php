<?php

class ldap extends Object {
    private $ldapConn = null;
    private $ldapServer = 'ldap://cup-master.cup.uni-muenchen.de';
    private $ldapPort = 389;
    private $baseDN = 'ou=people,ou=students-ch,dc=cup,dc=uni-muenchen,dc=de';

    public function __construct() {
        $this->ldapConn = ldap_connect($this->ldapServer, $this->ldapPort);
        ldap_set_option($this->ldapConn, LDAP_OPT_PROTOCOL_VERSION, 3);
    }

    public function auth($user, $pass) {
        if (empty($user) or empty($pass)) {
            return false;
        }
        $filter = "(&(objectClass=inetOrgPerson)(uid=$user))";
        $result = ldap_search($this->ldapConn, $this->baseDN, $filter);
        $info = ldap_get_entries($this->ldapConn, $result);
        $clientDN = $info[0]['dn'];
        debug($info);
        $good = ldap_bind($this->ldapConn, $clientDN, $pass);
        if( $good === true ) {
            return true;
        }
        return false;
    }

    public function __destruct() {
        ldap_unbind($this->ldapConn);
    }
}
?>
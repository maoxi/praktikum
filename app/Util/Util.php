<?php

function ensureArray(&$var) { // & = call by reference
    if(!isset($var))
        $var=array();
}

function rewriteType($analyseType=null){
    switch($analyseType){
        case 'alkali': return 'Alkali'; break;
        case 'erdalkali': return 'ErdAlkali'; break;
        case 'nhs': return '(NH4)2S'; break;
        case 'hs': return 'H2S'; break;
        case 'gesamt': return 'Gesamt'; break;
        case 'alkalimisch': return 'Alkali Misch'; break;
        case 'erdalkalimisch': return 'ErdAlkali Misch'; break;
        case 'nhsmisch': return '(NH4)2S Misch'; break;
        case 'hsmisch': return 'H2S Misch'; break;
        case 'gesamtmisch': return 'Gesamt Misch'; break;
        default: return'not found';
    }
}

?>
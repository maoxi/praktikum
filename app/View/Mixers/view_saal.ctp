<?php
/**
 * @var $this view
 */
?>

<?php

    $displayLevel1 = empty($this->Session->read('Auth.Belegung.id')) ? true : false;
    echo $this->element('mixer_action',array('saal_name'=>$this->Session->read('Auth.saal_name'),'visibility' => '' . $displayLevel1));
    $displayLevel2= empty($this->Session->read('Auth.Belegung.id'))? '75%':'25%';
    echo '<div class="level2" style="width:'.$displayLevel2.'">';
    echo '<h2>';
    echo 'Saal ' . $this->Session->read('Auth.Saal.name');
    echo '</h2>';
?>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('student_id'); ?></th>
            <th><?php echo $this->Paginator->sort('saal_id'); ?></th>
            <th><?php echo $this->Paginator->sort('Status'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($belegungs as $currentBelegung): ?>
            <?php
                $currentClass= $currentBelegung['Belegung']['id']==$this->Session->read('Auth.Belegung.id')? 'current':'';
                echo '<tr class="'.$currentClass.'">'
            ?>
                 <!--class="strtolower($currentBelegung['Belegung']['status']) == 'mistake' ? 'mistake' : ''; -->

                <td><?php echo h($currentBelegung['Student']['first_name'].' '.$currentBelegung['Student']['last_name']); ?>&nbsp;</td>
                <td><?php echo h($currentBelegung['Saal']['saal_name']); ?>&nbsp;</td>
                <td><?php echo h($currentBelegung['Belegung']['status']); ?>&nbsp;</td>
                <td class="actions">
                    <a class="showlevel3" id="<?php echo 'belegung_' . $currentBelegung['Belegung']['id']; ?>">Details...</a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>


<?php
foreach ($belegungs as $currentBelegung) {
    $classLevel3a = 'belegung_' . $currentBelegung['Belegung']['id'];
    $currentTests = $currentBelegung['Test'];
    $displayLevel3 = $this->Session->read('Auth.Belegung.id') == $currentBelegung['Belegung']['id'] ? "block" : "none";
    echo '<div class="level3" id="level3' . $classLevel3a . '" style="display: '.$displayLevel3.'" >';   //TODO change display here
    echo '<form method="post" id="' . $classLevel3a . '">';
    echo '<input type="hidden" class="' . $classLevel3a . '" name="belegung_id" value="' . $classLevel3a . '">';

    echo '<button class="belegung_' . $currentBelegung['Belegung']['id'] . ' saveBtn">Speichern</button>';
    echo '<table class=' . $classLevel3a . ' cellpadding="0" cellspacing="0">';
    echo '<tr>';
    echo '<th>Type</th>';
    echo '<th class="versuch">Versuch</th>';
    echo '<th class="probe">Probe</th>';
    echo '<th class="mixer">Mixer</th>';
    echo '<th>Student</th>';
    echo '<th class="correct">Ergebnis</th>';
    echo '</tr>';

    foreach ($currentTests as $currentTest) {
        $classLevel3b = $currentTest['analyseType'];//also used as $key in submit request
        $analyseType=$currentTest['analyseType'];
        $body = json_decode($currentTest['body'], true);

        echo '<tr>';
        echo '<td class="' . $classLevel3b . ' group_title" >' . rewriteType($analyseType) . '</td>';
        echo '<td colspan="5">';
        echo '<table class="' . $classLevel3b . '">';
        //debug($currentBody);
        $cntTry = count($body) == 1 ? 0 : count($body['try']);
        $cntFinishedTry = 0;  //whether the students have filled or not
        if ($cntTry == 0) {
            $cntFinishedTry = 0;
        } else {
            if (empty($body['try'][$cntTry - 1][0]['student'])) {   //TODO make sure that empty string is also saved
                $cntFinishedTry = $cntTry - 1;
            } else {
                $cntFinishedTry = $cntTry;
            }
        }

        if (strpos($analyseType, 'misch') == false) {                                                                  //Einzel Analyse
            //----show the history record in table

            for ($i = 0; $i < $cntFinishedTry; $i++) {
                for ($j = 0; $j < count($body['try'][$i]); $j++) {
                    echo '<tr>';
                    echo '<td class="versuch">'.($i + 1).'</td>';
                    echo '<td class="probe">'.($body['try'][$i][$j]['probe']+1).'</td>';
                    echo '<td class="mixer">' . $body['try'][$i][$j]['mixer'].'</td>';
                    echo '<td>' . $body['try'][$i][$j]['student'] . '</td>';
                    echo '<td class="correct">'.($body['try'][$i][$j]['correct']=='true'? '√':'×').'</td>' ;
                    echo '</tr>';
                }
            }

            if ($body['mixerTurn'] == 1) {
                //----create the first try
                if ($cntFinishedTry == 0) {
                    for ($i = 0; $i < 3; $i++) {
                        echo '<tr><td class="versuch">1</td>';
                        echo '<td class="probe">'.($i + 1).'</td>';
                        echo '<td class="mixer">' . $this->Form->input($classLevel3b . '0_'.$i, array('label' => false, 'default' => '', 'required' => false, 'class' => $classLevel3b));
                        echo '</td><td></td><td></td></tr>';
                    }
                } else {
                    //cnt the mistake from student
                    for ($i = 0; $i < count($body['try'][$cntFinishedTry-1]); $i++) {
                        if ($body['try'][$cntFinishedTry-1][$i]['correct'] == 'false') {
                            //only create felds for mistakes
                            echo '<tr><td class="versuch">' . ($cntFinishedTry + 1).'</td>';
                            echo '<td class="probe">'.($i + 1).'</td><td class="mixer">';
                            echo $this->Form->input($classLevel3b.($cntFinishedTry + 1).'_'.$i,
                                array('label' => false, 'default' => '', 'required' => false, 'class' => $classLevel3b));
                            echo '</td><td></td><td></td></tr>';
                        }
                    }
                }
            } else if ($body['mixerTurn'] == 0) {  //$cntTry always= $cntFinishedTry+1
                /*if ($cntTry == 0) {
                    for ($i = 0; $i < 3; $i++) {
                        echo '<tr><td class="versuch">1</td><td class="mixer">';
                        echo '<tr><td class="probe">'.($i + 1).'</td><td class="mixer">';
                        echo $this->Form->input($classLevel3b.'0_'.$i, array('label' => false, 'default' => '', 'required' => false, 'class' => $classLevel3b));
                        echo '</td><td></td><td></td></tr>';
                    }
                } else*/
                if ($cntTry == 1) {
                    for ($i = 0; $i < 3; $i++) {  //if$cntTry==1, no need to count 'falsch', 3 input at the begin
                        echo '<tr><td class="versuch">1</td>';
                        echo '<td class="probe">'.($i + 1).'</td><td class="mixer">';
                        echo $this->Form->input($classLevel3b.($cntTry - 1) . '_' . $i,
                            array('default' => $body['try'][$cntTry - 1][$i]['mixer'], 'label' => false, 'required' => false, 'readonly' => 'readonly','class' => $classLevel3b));
                        echo '<button class="reedit_einzel" type="button">X</button>';
                        echo '</td><td></td><td></td></tr>';
                    }
                } else {
                    for ($i = 0; $i < count($body['try'][$cntTry - 1]); $i++) {
                            echo '<tr><td class="versuch">'.$cntTry .'</td>';
                            echo '<td class="probe">'.($body['try'][$cntTry - 1][$i]['probe']+1).'</td><td class="mixer">';
                            echo $this->Form->input($classLevel3b.($cntTry - 1).'_'.$body['try'][$cntTry - 1][$i]['probe'],
                                array('default' => $body['try'][$cntTry - 1][$i]['mixer'], 'label' => false, 'required' => false, 'readonly' => 'readonly','class' => $classLevel3b));
                            echo '<button class="reedit_einzel" type="button">X</button>';
                            echo '</td><td></td><td></td></tr>';
                    }
                }
            }
        } else {
            if ($cntFinishedTry > 0) {
                $mischMixerInput = $body['try'][0][0]['mixer'];
                //-----put the history in table
                for ($i = 0; $i < $cntFinishedTry; $i++) {
                    if (!empty($body['try'][$i][0]['student'])) {
                        echo '<tr><td class="versuch">' . ($i + 1) .'</td>';
                        echo '<td class="probe">'.'1'.'</td>';
                        if($i==0){  //only show the mixer input once
                            echo '<td class="mixer" rowspan="'.$cntFinishedTry.'">';
                            echo '<p>' . $mischMixerInput['nummer'] . '</p>';
                            echo '<p>' . $mischMixerInput['salz'] . '</p>';
                            echo '<p> Kationen: ' . $mischMixerInput['kationen'] . '</p>';
                            echo '<p> Anionen: ' . $mischMixerInput['anionen'] . '</p>';
                            echo '</td>';
                        }
                        echo '<td>';
                        echo '<p>' . $body['try'][$i][0]['student']['kationen'] . '</p>';
                        echo '<p>' . $body['try'][$i][0]['student']['anionen'] . '</p>';
                        echo '</td>';
                        echo '<td class="correct">'.($body['try'][$i][0]['correct']['correct']=='true'? '√':'×').'</td>' ;
                        echo '</tr>';
                    }
                }
            }

            if ($body['mixerTurn'] == 1) {
                if ($cntTry == 0) {   //totally the beginning
                    echo '<tr><td class="versuch">1</td>';
                    echo '<td class="probe">'.'1'.'</td>';
                    echo '<td class="mixer"><div class="updateInput"></div>';
                    echo $this->Form->input($classLevel3b, array(
                        'options' => $mischanalysesList,
                        'empty' => 'Gruppe wählen',
                        'label' => false,
                        'class' => $classLevel3b . ' myDropDown'
                    ));
                    echo $this->Form->input('', array('label' => 'Kationen:', 'default' => '', 'readonly' => true, 'class' => 'kationen disabledMixerInput'));
                    echo $this->Form->input('', array('label' => 'Anionen:', 'default' => '', 'readonly' => true, 'class' => 'anionen disabledMixerInput'));
                    echo '</td> <td></td><td></td></tr>';
                }
            } elseif ($body['mixerTurn'] == 0) {
                $mischMixerInput = $body['try'][0][0]['mixer'];
                if ($cntFinishedTry == 0) {      //$cntTry==1, students havent input yet, and mixer has inputted
                    echo '<tr><td class="versuch">1</td>';
                    echo '<td class="probe">'.'1'.'</td>';
                    echo '<td class="mixer"><div class="updateInput">' . $mischMixerInput['salz'] . '</div>';
                    echo $this->Form->input($classLevel3b, array(
                        'options' => $mischanalysesList,
                        'default' => $mischMixerInput['nummer'],
                        'empty' => 'Gruppe Wählen',
                        'label' => false,
                        'class' => $classLevel3b . ' myDropDown',
                        'disabled'=>'disabled'
                    ));
                    echo '<button class="reedit_misch" type="button">X</button>';
                    echo $this->Form->input('', array('label' => 'Kationen:', 'default' => '' . $mischMixerInput['kationen'], 'readonly' => true, 'class' => 'kationen disabledMixerInput'));
                    echo $this->Form->input('', array('label' => 'Anionen:', 'default' => '' . $mischMixerInput['anionen'], 'readonly' => true, 'class' => 'anionen disabledMixerInput'));
                    echo '</td><td></td><td></td></tr>';
                }
            }
        }
        echo '</table>';
        echo '</td>';
        echo '</tr>';
    }
    echo '</table>';
    echo '</form>';
    echo '</div>';
}?>


<script>
    $(document).ready(function () {
        var $focused;
        $('input').each(function () {
            var input = $(this);
            input.autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<?php echo $this->Html->url(array('action' => 'ionsautocomplete', 'ext' => 'json')); ?>',
                        dataType: 'json',
                        data: {
                            'term': input.val(),
                            'gType': input.closest('.table').attr('class')
                        },
                        success: function (data) {
                            response(data.allItems);
                        },
                        minLength: 1
                    });
                }
            });
        });

        $('input.ui-autocomplete-input[type=text]').focusin(showIonTable);
        $('body').click(hideIonTable);
        $('div.myButtons button[type=submit]').click(changeTableFocus);
        $('button.reedit_einzel').click(deleteEinzelInput);
        $('button.reedit_misch').click(deleteMischInput);

        //myButtons (css= certain width)
        $('div.myButtons button').each(function () {
            var input = $(this);
            if (input.width() < 110) {
                input.css('width', '110px')
            } else {
                input.css('width', '220px');
            }
        });

        //update the mixer input if text in dropdown list changed
        $('select').change(function () {
            var selected = $(this);
            $.ajax({
                type: 'POST',
                url: '<?php echo $this->Html->url(array('action' => 'inputUpdate', 'ext' => 'json')); ?>',
                dataType: 'json',
                data: {
                    'term': selected.val()
                },
                success: function (result) {
                    if (!jQuery.isEmptyObject(result.allItems)) {
                        if (result.allItems.length > 2) {
                            selected.closest('td').find('div.updateInput').text(result.allItems[1]);
                            selected.closest('td').find('input:eq(0)').val(result.allItems[2]).prop('hidden', false);
                            selected.closest('td').find('input:eq(1)').val(result.allItems[3]).prop('hidden', false);
                        } else {
                            selected.closest('td').find('div.updateInput').text('');
                            selected.closest('td').find('input:eq(0)').val('').prop('hidden', false);
                            selected.closest('td').find('input:eq(1)').val('').prop('hidden', false);
                        }
                    }
                },
                minLength: 1
            });
        });

        //mistakes print in red
        $("table table td").each(function () {
            var input = $(this);
            if (input.html() == 'false') {
                input.css({"color": "#aa0000", "font-weight": "bold"});
            }
        });

        $('div.level2').click(level2);
        $('.saveBtn').click(saveMixerResult);
        $('input.kationen, input.anionen').prop('disabled',true);

    });

</script>

<div class="mixers form">
<?php echo $this->Form->create('Mixer'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Mixer'); ?></legend>
	<?php
		echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
		echo $this->Form->input('pwd');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
        <li><?php echo $this->Html->link('Eine neue Übung starten', array('controller'=>'kurses','action'=>'add'))?></li>
        <li><?php echo $this->Html->link('Zuweisung des Mixers',array('controller'=>'belegungs','action'=>'index'))?></li>
        <li><?php echo $this->Html->link('List Students',array('controller'=>'students','action'=>'index'))?></li>
        <li><?php echo $this->Html->link('List Kurses',array('controller'=>'kurses','action'=>'index'))?></li>
        <li><?php echo $this->Html->link('List Mixers',array('controller'=>'mixers','action'=>'index'))?></li>
	</ul>
</div>

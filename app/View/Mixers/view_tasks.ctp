<?php
/**
 * @var $this view
 */
?>

<?php
echo $this->element('mixer_action',array('saal_name'=>$this->Session->read('Auth.saal.name')));
?>

<div class="mixers view">
<h2><?php echo 'Übersicht'; ?></h2>

    <table cellpadding="0" cellspacing="0">
        <thead>

        <tr>
            <th><?php echo $this->Paginator->sort('student_id','Student'); ?></th>
            <th><?php echo $this->Paginator->sort('saal_id','Saal'); ?></th>
            <th><?php echo $this->Paginator->sort('punkt','Punkte'); ?></th>
            <th><?php echo $this->Paginator->sort('analyseType','Analysen noch zu bearbeiten!'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($belegungs as $belegung): ?>
            <tr>
                <td><?php echo h($belegung['Student']['first_name'].' '.$belegung['Student']['last_name']); ?>&nbsp;</td>
                <td><?php echo h($belegung['Saal']['saal_name']); ?>&nbsp;</td>
                <td><?php echo h($belegung['Belegung']['punkt']); ?>&nbsp;</td>
                <td>
                    <table>
                        <?php
                            $allFinished=true;
                            foreach($belegung['Test'] as $eachTest){
                                $body=json_decode($eachTest['body'],true);
                                if(!strpos($eachTest['analyseType'],'misch')){ //einzel
                                    if($body['mixerTurn']!=2){
                                        echo '<tr><td>'.rewriteType($eachTest['analyseType']).'</td></tr>';
                                        $allFinished=false;
                                    }
                                }else{                                                      //mischt
                                    if(!isset($body['try'][0][0]['mixer'])){
                                        echo '<tr><td>'.rewriteType($eachTest['analyseType']).'</td></tr>';
                                        $allFinished=false;
                                    }
                                }
                            }
                            if(!isset($belegung['Kurse']['id'])){
                                echo '<tr><td>Es ist noch kein Kurs angelegt</td></tr>';
                            }elseif(isset($belegung['Kurse']['id']) && $allFinished==true){
                                echo '<tr><td>Alle fertig</td></tr>';
                            }
                        ?>
                    </table>
                </td>
                <td class="actions">
                    <?php echo $this->Html->link('Edit...', array('controller'=>'mixers','action' => 'view_saal', '?' => array('saal_name' => $belegung['Saal']['saal_name'], 'belegung_id' => $belegung['Belegung']['id']))); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>



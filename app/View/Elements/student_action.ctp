<?php
if (!isset($visibility) || $visibility == true) {
    echo '<div class="actions level1" style="display: block">';
} else {
    echo '<div class="actions level1" style="display: none">';
}

echo '<h3>Auswahl</h3>';
echo '<ul>';

$class = ('Analysen' === $this->Session->read('Auth.currentAction') ? "current_action" : "");
echo '<li class="' . $class . '">';
echo $this->Html->link('Analysen ', array('controller' => 'students', 'action' => 'view_kurse'));
echo '</li>';

$class = ('Punkteübersicht' === $this->Session->read('Auth.currentAction') ? "current_action" : "");
echo '<li class="' . $class . '">';
echo $this->Html->link('Punkteübersicht', array('controller' => 'students', 'action' => 'view_scores'));
echo '</li>';

echo '</ul>';
echo '</div>';
?>
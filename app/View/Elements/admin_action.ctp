<div class="actions level1">
    <h3><?php echo __('Auswahl'); ?></h3>
    <ul>
        <li class="<?php if($this->Session->read('Auth.currentAction')==='Student & Note') echo 'current_action';?>">
            <?php echo $this->Html->link('Student & Note',array('controller'=>'students','action'=>'admin_index'))?>
        </li>
        <li class="<?php if($this->Session->read('Auth.currentAction')==='Saal Belegung') echo 'current_action'; ?>">
            <?php echo $this->Html->link('Saal Belegung',array('controller'=>'belegungs','action'=>'admin_index'))?>
        </li>
        <li class="<?php if($this->Session->read('Auth.currentAction')==='Neuen Kurs starten') echo 'current_action';?>">
            <?php echo $this->Html->link('Neuen Kurs starten', array('controller'=>'kurses','action'=>'admin_add'))?>
        </li>
        <li class="<?php if($this->Session->read('Auth.currentAction')==='Statistiken') echo 'current_action';?>">
            <?php echo $this->Html->link('Statistiken', array('controller'=>'students','action'=>'admin_view_statistik'))?>
        </li>
    </ul>
</div>
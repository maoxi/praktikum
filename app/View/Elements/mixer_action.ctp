<?php
    if(!isset($visibility) || $visibility==true){
        echo '<div class="actions level1" style="display: block">';
    }else{
        echo '<div class="actions level1" style="display: none">';
    }
    echo '<h3>Auswahl</h3>';
    echo '<ul>';
         foreach($saalNames as $saalName){
            $class = ('Saal '.$saalName=== $this->Session->read('Auth.currentAction') ? "current_action" : "");
            echo '<li class='.$class.'>' ;
            echo $this->Html->link('Saal '.$saalName , array('controller' => 'mixers', 'action' => 'view_saal', '?'=>array('saal_name'=>$saalName)));
            echo '</li>';
        }
        $class=('Übersicht'=== $this->Session->read('Auth.currentAction') ? "current_action" : "");

        echo '<li class="'.$class.'">';
            echo $this->Html->link('Übersicht', array('controller' => 'mixers', 'action' => 'view_tasks'));
        echo '</li>';
    echo '</ul>';
echo '</div>';
?>
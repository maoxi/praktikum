<?php
/**
 * @var $this view
 */
?>
<div class="actions level1">
    <h3><?php echo __('Auswahl'); ?></h3>
    <ul>
        <li class="<?php if($this->Session->read('Auth.currentAction')==='Saal Belegung') echo 'current_action'; ?>">
            <?php echo $this->Html->link('Saal Belegung',array('controller'=>'assistents','action'=>'view_saal'))?>
        </li>
    </ul>
</div>
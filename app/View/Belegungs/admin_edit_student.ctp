<?php
echo $this->element('admin_action');
?>

<div class="belegungs form">
<?php echo $this->Form->create('Belegung'); ?>
	<fieldset>
		<legend><?php echo __('Saalwechsel'); ?></legend>
	<?php
		echo $this->Form->input('id');
        echo $this->Form->input('student_id',array('options'=>$students,'disabled'=>true));
        echo $this->Form->input('saal_id',array('options'=>$saals));

    debug($students);
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>


<script>
    $(document).ready(function(){
        $('form').bind('submit', function() {
            $(this).find(':input').removeAttr('disabled');
        });
    }
</script>
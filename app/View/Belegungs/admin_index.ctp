<?php
echo $this->element('admin_action');
?>

<div class="belegungs index">
	<h2><?php echo __('Saal Belegung');?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('student_id'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
            <th><?php echo $this->Paginator->sort('saal_id'); ?></th>
            <th><?php echo 'Assistent'; ?></th>
        <th><?php echo $this->Paginator->sort('mixer_id'); ?></th>
        <th class="actions"><?php echo __('Actions'); ?></th>

	</tr>
	</thead>
	<tbody>
	<?php foreach ($belegungs as $belegung): ?>
	<tr>
		<td><?php echo h($belegung['Student']['first_name'].' '.$belegung['Student']['last_name']); ?>&nbsp;</td>
        <td class="actions">
            <?php echo $this->Html->link(__('Saalwechsel'), array('action' => 'edit_student', $belegung['Belegung']['id'])); ?>
        </td>
        <td><?php echo h($belegung['Saal']['saal_name']); ?>&nbsp;</td>
        <td><?php echo h($belegung['Saal']['Assistent']['first_name'].' '.$belegung['Saal']['Assistent']['last_name']); ?>&nbsp;</td>
        <td><?php echo h($belegung['Saal']['Mixer']['first_name'].' '.$belegung['Saal']['Mixer']['last_name']); ?>&nbsp;</td>
        <td class="actions">
            <?php echo $this->Html->link(__('Mixerwechsel'), array('action' => 'edit_mixer', $belegung['Saal']['id'])); ?>
        </td>

	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>


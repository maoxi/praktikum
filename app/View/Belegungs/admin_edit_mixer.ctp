<?php
echo $this->element('admin_action');
?>

<div class="saals form">
<?php echo $this->Form->create('Saal'); ?>
	<fieldset>
		<legend><?php echo __('Mixerwechsel'); ?></legend>
	<?php
        $current_saal=$saal['Saal']['saal_name'];
		echo $this->Form->input('id');
        echo $this->Form->input('saal_id',array('empty'=>$current_saal,'options'=>$saals,'disabled'=>true));
        echo $this->Form->input('mixer_id',array('options'=>$mixers));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>


<script>
    $(document).ready(function(){
        $('form').bind('submit', function() {
            $(this).find(':input').removeAttr('disabled');
        });
    }
</script>
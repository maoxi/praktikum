<?php
echo $this->element('admin_action');
?>

<div class="kurses form">
    <?php
    if(!empty($kurses)){
        echo '<h2> Aktueller Kurs </h2>';
        echo '<div class="input text"><label>Name</label><input disabled="disabled" value="'.$kurses[0]['Kurse']['name'].'"></div>';
        echo '<div class="input date"><label>Created Date</label><input disabled="disabled" value="'.$kurses[0]['Kurse']['created_date'].'"></div>';
        //echo '<button class="deleteBtn">Delete</button>';
        echo $this->Html->link('Delete',
            array('action' => 'delete', $kurses[0]['Kurse']['id']),
            array('class'=>'button deleteBtn'),
            'Are you sure to delete'
        );
    }else{
        echo $this->Form->create('Kurse');
        echo '<h2> Neuen Kurs starten </h2>';
        echo $this->Form->input('name', array('placeholder' => 'Z.B. ss2015-1'));
        echo $this->Form->input('created_date');
        echo $this->Form->end(__('Submit'));
    }
    ?>
</div>



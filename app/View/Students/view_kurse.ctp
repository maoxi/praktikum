<?php
/**
 * @var $this view
 */
?>
<?php
    $displayLevel1 = empty($this->Session->read('Auth.Test.id')) ? true : false;
    echo $this->element('student_action', array('cnt_kurses' => $this->Session->read('Auth.cnt_kurses'), 'visibility' => '' . $displayLevel1));

    $displayLevel2 = empty($this->Session->read('Auth.Test.id')) ? '75%' : '25%';
    echo '<div class="level2" style="width:' . $displayLevel2 . '">';

    if (!isset($belegungs[0]['Kurse']['id'])) {
        echo '<h2>Es ist noch kein Kurs angelegt!</h2>';
    } else {
        echo '<h2>Analysen</h2>';
        echo '<h2 class="group">Einzelanalysen</h2>';
        foreach ($belegungs[0]['Test'] as $key => $test) {
            $classLevel2 = $test['analyseType'];
            $body = json_decode($test['body'], true);

            if ($classLevel2 == 'alkalimisch') {
                echo '<h2 class="group">Mischanalysen</h2>';
            }

            if (!isset($body['try'])) {                                                                       //if totally empty
                $div = $this->Html->div("", rewriteType($classLevel2), array('id' => $classLevel2));
                echo '<a class="no_kurse">' . $div . '</a>';
            } else {
                if (strpos($test['analyseType'], 'misch')) {                                       //misch Analyse
                    $entsprechendeEinzel = json_decode($belegungs[0]['Test'][$key - 5]['body'], true);
                    if (($entsprechendeEinzel['mixerTurn'] !== 2)) {                                                //entsprechende einzel ist noch nicht abgeschlossen
                        $div = $this->Html->div("", rewriteType($classLevel2), array('id' => $classLevel2));
                        echo '<a class="no_kurse">' . $div . '</a>';
                    } else {                                                                                      //entsprechned einzel ist schon abgeschlossen
                        $notFinished = $body['mixerTurn'] == 2 ? '' : '_notFnshed';
                        $div = $this->Html->div("showlevel3", rewriteType($classLevel2), array('id' => $classLevel2));
                        echo '<a class="opened_kurse' . $notFinished . '">' . $div . '</a>';
                    }
                } else {                                                                                     // Einzel Analyse
                    $notFinished = $body['mixerTurn'] == 2 ? '' : '_notFnshed';
                    $div = $this->Html->div("showlevel3", rewriteType($classLevel2), array('id' => $classLevel2));
                    echo '<a class="opened_kurse' . $notFinished . '">' . $div . '</a>';
                }
            }
        }
    }
?>
</div>

<?php foreach ($belegungs[0]['Test'] as $currentTest) {
    $classLevel3 = $currentTest['analyseType']; //delete the empty string in test name
    $displayLevel3 = $this->Session->read('Auth.Test.id') == $currentTest['id'] ? "block" : "none";   //whether the level3 should be shown after submit
    echo '<div class="level3" id="level3' . $classLevel3 . '" style="display: ' . $displayLevel3 . '">';
    echo '<form method="post" id="' . $classLevel3 . '">';

    echo '<fieldset>';
    $body = json_decode($currentTest['body'], true);
    echo '<a class="note"> Punkte:' . $currentTest['punkt'] . '</a>';
    //input used for submit test_id and kurse_id
    echo '<input type="hidden" class="' . $classLevel3 . '" name="test_id" value="' . $currentTest['id'] . '">';
    echo '<input type="hidden" class="' . $classLevel3 . '" name="kurse_id" value="' . $this->Session->read('Auth.Kurse.id') . '">';

    echo '<table>';
    //from 2 start the entries of 'try'
    $cnt_Try = count($body) == 1 ? 0 : count($body['try']);
    if (!strpos($classLevel3, 'misch')) {                                                     //einzel Analyse
        echo $this->Html->tableHeaders(array('Versuch', 'Probe', 'Ihre Antwort', 'Ergebnis'));
        for ($i = 0; $i < $cnt_Try; $i++) {
            for ($j = 0; $j < count($body['try'][$i]); $j++) {
                if (strlen(trim($body['try'][$i][$j]['correct'])) > 0) {                                                    //both have filled
                    echo $this->Html->tableCells(array(($i + 1), $body['try'][$i][$j]['probe'] + 1, $body['try'][$i][$j]['student'], ($body['try'][$i][$j]['correct'] == 'true' ? '√' : '×')));
                } else if (strlen(trim($body['try'][$i][$j]['correct'])) == 0 && strlen(trim($body['try'][$i][$j]['mixer'])) > 0) {
                    echo '<tr><td class="versuch">' . ($i + 1) . '</td>';
                    echo '<td class="probe">' . ($body['try'][$i][$j]['probe'] + 1) . '</td><td class="student">';
                    echo $this->Form->input($j, array('default' => '', 'label' => false, 'required' => false, 'class' => $classLevel3 . ' autoComplete '));
                    echo '</td> <td></td></tr>';
                }//else mixer hasnt filled yet
            }
        }
    } else {
        echo $this->Html->tableHeaders(array('Versuch', 'Ihre Antwort', 'Ergebnis'));             //misch analyse
        $gesamtfehlerzahl = 0;
        echo '<tr>';                                                                                                //eingabe beispiel
        echo '<td>Beispiel</td>';
        echo '<td>';
        echo 'Kationen';
        echo '<div class="beispiel">k1,k2,...</div>';
        echo 'Anionen';
        echo '<div class="beispiel" ">a1,a2,...</div>';
        echo '</td>';
        echo '<td>Ionen mit Trennen oder Leerzeichen. </br></br> Feld frei lassen falls nicht zutreffend.</td>';
        echo '</tr>';
        for ($i = 0; $i < $cnt_Try; $i++) {
            if (!empty($body['try'][$i][0]['correct'])) {                                                             //both have filled
                $correctness = $body['try'][$i][0]['correct'];
                echo '<tr><td class="versuch">' . ($i + 1) . '</td><td class="student">';
                echo 'Kationen: ' . $body['try'][$i][0]['student']['kationen'] . '</br>';
                echo 'Anionen: ' . $body['try'][$i][0]['student']['anionen'];
                echo '</td> <td>';
                echo ($correctness['correct'] == 'true' ? 'Richtig' : 'Falsch') . '</br>';
                echo $correctness['kZuwenig'] . ' Kation zu wenig </br>';
                echo $correctness['kZuviel'] . ' Kation zu viel </br>';
                echo $correctness['aZuwenig'] . ' Anion zu wenig </br>';
                echo $correctness['aZuviel'] . ' Anion zu viel </br>';
                echo '</td></tr>';
                $gesamtfehlerzahl = $gesamtfehlerzahl + (int)$correctness['kZuwenig'] + (int)$correctness['kZuviel'] + (int)$correctness['aZuwenig'] + (int)$correctness['aZuviel'];
            } else if (empty($body['try'][$i][0]['correct']) && isset($body['try'][$i][0]['mixer']) > 0) {               //mixer has filled, student hasnt
                echo '<tr><td>' . ($i + 1) . '</td> <td>';
                echo $this->Form->input('kationen', array('default' => '', 'label' => 'kationen', 'required' => false, 'class' => $classLevel3));
                echo $this->Form->input('anionen', array('default' => '', 'label' => 'anionen', 'required' => false, 'class' => $classLevel3));
                echo '</td> <td>Bitte gesamte Analyse eingeben</td></tr>';
            }//else mixer hasnt filled yet
        }
        echo '<tr>';
        echo '<td></td><td></td><td><strong>Gesamtfehlerzahl: ' . $gesamtfehlerzahl . '</strong></td>';
        echo '</tr>';
    }
    echo '</table>';

    echo '<button class="' . $classLevel3 . ' btn_std_save_test" style ="display:none;" >Save</button >';
    echo '<button class="' . $classLevel3 . ' confirmBtn" style="display:none" >Confirm</button>';
    echo '</form>';
    echo '</fieldset >';
    echo '</div >';
}?>


<script>
    $(document).ready(function () {
        $('input.autoComplete').each(function () {
            var input = $(this);
            input.autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<?php echo $this->Html->url(array('action' => 'ionsautocomplete', 'ext' => 'json')); ?>',
                        dataType: 'json',
                        data: {
                            'term': input.val()
                        },
                        success: function (data) {
                            response(data.allItems);
                        },
                        minLength: 1
                    });
                }
            });
        });

        //mistakes print in red
        $("table td").each(function () {
            var input = $(this);
            if (input.html().indexOf('false') > -1) {
                input.css({"color": "#aa0000", "font-weight": "bold"});
            }
        });

        $('div.level2').click(level2);
        $('input[type=text]').keydown(enableSaveBtn);
        $('.btn_std_save_test').click(enableEditBtn);
        $('.confirmBtn').click(saveResult);

        //auto input for students
        $('input.ui-autocomplete-input[type=text]').focusin(showIonTable);
        $('body').click(hideIonTable);
        $('div.myButtons button[type=submit]').click(changeTableFocus);
        //myButtons (css= certain width)
        $('div.myButtons button').each(function () {
            var input = $(this);
            if (input.width() < 110) {
                input.css('width', '110px')
            } else {
                input.css('width', '220px');
            }
        });
    });

</script>
<?php
/**
 * @var $this view
 */
?>
<?php
echo $this->Html->script('d3.min.js');
echo $this->Html->script('einzelStatistik');
echo $this->Html->script('stackedStatistik');
echo $this->Html->script('stackedStatistikY');
echo $this->Html->script('noteStatistik');
//echo $this->Html->script('http://labratrevenge.com/d3-tip/javascripts/d3.tip.v0.6.3.js');

echo $this->element('admin_action', array('cnt_kurses' => $this->Session->read('Auth.cnt_kurses')));

echo '<div class="students form">';
echo '<h2>Statistiken</h2>';
?>
<nav  id="tabs">
    <?php
        echo '<ul>';
        foreach($saals as $saal){
            echo '<li><a href="#tabs-'.$saal['Saal']['id'].'">Saal '.$saal['Saal']['saal_name'].'</a></li>';
        }
        echo '<li><a href="#tabs-'.(count($saals)+1).'">Zusammen</a></li>';
        echo '<li><a href="#tabs-'.(count($saals)+2).'">Note</a></li>';
        echo '<li><a href="#tabs-'.(count($saals)+3).'">Schwerigkeit</a></li>';
        echo '</ul>';

        foreach($saals as $saal){
            echo '<div id="tabs-'.$saal['Saal']['id'].'">';
            echo '<p class="saal'.$saal['Saal']['id'].'"></p>';
            echo '</div>';
        }
    ?>
    <div id="tabs-7">
        <p class="zusammen"></p>
    </div>
    <div id="tabs-8">
        <p class="note"></p>
    </div>
    <div id="tabs-9">
        <label class='schwerigkeit'><input type="checkbox"> <span>Sortieren nach Fehlerrate (sonst nach Vorkommen)</span></label>
        <p class="schwerigkeit"></p>
    </div>
</nav >


<script>
    $(function() {
        $( "#tabs" ).tabs({
            event: "mouseover"
        });
    });


    var saalPunkt= <?php echo json_encode($saalPunkte[0])?>;
    drawEinzelStatistik(saalPunkt,'p.saal1');
    drawStackedStatistik(saalPunkt,'p.saal1');
    var saalPunkt= <?php echo json_encode($saalPunkte[1])?>;
    drawEinzelStatistik(saalPunkt,'p.saal2');
    drawStackedStatistik(saalPunkt,'p.saal2');
    var saalPunkt= <?php echo json_encode($saalPunkte[2])?>;
    drawEinzelStatistik(saalPunkt,'p.saal3');
    drawStackedStatistik(saalPunkt,'p.saal3');
    var saalPunkt= <?php echo json_encode($saalPunkte[3])?>;
    drawEinzelStatistik(saalPunkt,'p.saal4');
    drawStackedStatistik(saalPunkt,'p.saal4');
    var saalPunkt= <?php echo json_encode($saalPunkte[4])?>;
    drawEinzelStatistik(saalPunkt,'p.saal5');
    drawStackedStatistik(saalPunkt,'p.saal5');
    var saalPunkt= <?php echo json_encode($saalPunkte[5])?>;
    drawEinzelStatistik(saalPunkt,'p.saal6');
    drawStackedStatistik(saalPunkt,'p.saal6');

    var zusammen = <?php echo json_encode($zusammen); ?>;
    drawEinzelStatistik(zusammen,"p.zusammen");
    //zusammen = <?php echo json_encode($zusammen); ?>;
    drawStackedStatistik(zusammen,"p.zusammen");

    var noteVerteilung = <?php echo json_encode($noteVerteilung); ?>;
    drawNoteStatistik(noteVerteilung,"p.note");

    var schwerigkeit = <?php echo json_encode($schwerigkeit); ?>;
    drawStackedStatistikY(schwerigkeit,"p.schwerigkeit");

</script>



<?php
echo $this->element('admin_action');
?>

<?php
echo '<div class="level2">';
echo '<h2> Student & Note</h2>';
echo $this->Html->link('Alle speichern', array('action' => 'save_excels'), array('class' => 'button', 'target' => '_blank')); ?>

<table cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th><?php echo $this->Paginator->sort('first_name'); ?></th>
        <th><?php echo $this->Paginator->sort('last_name'); ?></th>
        <th><?php echo $this->Paginator->sort('martikelNr'); ?></th>
        <th><?php echo 'Punkt' ?></th>
        <th><?php echo 'Note' ?></th>
        <th class="actions"><?php echo __('Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($belegungs as $belegung): ?>
        <tr>
            <td><?php echo h($belegung['Student']['first_name']); ?>&nbsp;</td>
            <td><?php echo h($belegung['Student']['last_name']); ?>&nbsp;</td>
            <td><?php echo h($belegung['Student']['martikelNr']); ?>&nbsp;</td>
            <td><?php
                if (isset($belegung['Belegung']['punkt']))
                    echo $belegung['Belegung']['punkt'];
                else
                    echo '-';
                ?></td>
            <td><?php echo h($belegung['Belegung']['note']); ?>&nbsp;</td>
            <td class="actions">
                <a class="showlevel3" id='student_<?php echo $belegung['Student']['id']; ?>'>Detail...</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<p>
    <?php
    echo $this->Paginator->counter(array(
        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
    ));
    ?>    </p>
<div class="paging">
    <?php
    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
</div>
</div>


<?php foreach ($belegungs as $belegung): ?>
<div class="level3" id="level3student_<?php echo $belegung['Student']['id']; ?>" style="display:none">
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th><?php echo 'AnalyseType'; ?></th>
            <th><?php echo 'Versuch'; ?></th>
            <th><?php echo 'Student eingabe'; ?></th>
            <th><?php echo 'Mixer eingabe'; ?></th>
            <th><?php echo 'Korrekt'; ?></th>
            <th><?php echo 'Punkte'; ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($belegung['Test'] as $test) {
            echo '<tr>';
            echo '<td>' . rewriteType($test['analyseType']) . '</td>';
            echo '<td colspan="4">';
            $body = json_decode($test['body'], true);
            if (isset($body['try'])) {
                $cntTry = count($body) == 1 ? 0 : count($body['try']);
                if ($cntTry > 0) {
                    echo '<table>';
                    for ($i = 0; $i < $cntTry; $i++) {
                        for ($j = 0; $j < count($body['try'][$i]); $j++) {
                            echo '<tr>';
                            echo '<td>' . ($i + 1) . '.te' . '</td>';
                            if (strpos($test['analyseType'], 'misch')) {
                                $studentInput = $body['try'][$i][$j]['student'];
                                $mixerInput = $body['try'][$i][$j]['mixer'];
                                if (!empty($studentInput)) {
                                    echo '<td>' . $studentInput['kationen'] . ' </br>' . $studentInput['anionen'] . '&nbsp;</td>';
                                    echo '<td>' . $mixerInput['kationen'] . ' </br>' . $mixerInput['anionen'] . '&nbsp;</td>';
                                } else {
                                    echo '<td> bearbeiten gerade &nbsp;</td>';
                                    echo '<td>' . $mixerInput['kationen'] . ' </br>' . $mixerInput['anionen'] . '&nbsp;</td>';
                                }
                            } else { //einzel analyse
                                //if(!empty($body['try'][$i][$j]['student'])){
                                echo '<td>' . $body['try'][$i][$j]['student'] . '&nbsp;</td>';
                                echo '<td>' . $body['try'][$i][$j]['mixer'] . '&nbsp;</td>';
                                //}
                            }
                            if (strpos(json_encode($body['try'][$i][$j]['correct']), 'true')) {
                                echo '<td> √ </td>';
                            } elseif (!empty($body['try'][$i][$j]['correct'])) {
                                echo '<td> × </td>';
                            }
                            echo '</tr>';
                        }
                    }
                    echo '</table>';
                }
            }
            echo '</td>';
            if (is_null($test['punkt'])) {
                echo '<td>Not finished</td>';
            } else
                echo '<td>' . $test['punkt'] . '</td>';

            echo '</tr>';
        }
        ?>
        </tbody>
    </table>
    </div>
    <?php endforeach; ?>

    <script>

        $(document).ready(function () {
            $('div.level2').click(level2);
        });

    </script>
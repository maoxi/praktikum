<?php
/**
 * @var $this view
 */
?>
    <div class="stdTable">
        <table>
            <tr>
                <th><?php echo 'Name';?></th>
                <th><?php echo 'MartikelNr';?></th>
                <th><?php echo 'Gesamt punkt';?></th>
                <th><?php echo 'Note'; ?></th>
                <th><?php echo 'AnalyseType'; ?></th>
                <th><?php echo 'Versuch';?></th>
                <th><?php echo 'Student eingabe';?></th>
                <th><?php echo 'Mixer eingabe';?></th>
                <th><?php echo 'Korrekt';?></th>
                <th><?php echo 'Punkt';?></th>
            </tr>
            <?php foreach ($belegungs as $belegung){
                echo '<tr>';
                echo '<td>'.$belegung['Student']['first_name'].' '.$belegung['Student']['last_name'].'</td>';
                echo '<td>'.$belegung['Student']['martikelNr'].'</td>';
                if(is_null($belegung['Belegung']['punkt'])){   //TODO here
                    echo '<td>Not finished</td>';
                }else
                    echo '<td>'.$belegung['Belegung']['punkt'].'</td>';
                echo '<td>'.$belegung['Belegung']['note'].'</td>';
                echo '<td colspan="6">';
                if(count($belegung['Test'])>0){
                    echo '<table>';
                    foreach ($belegung['Test'] as $test) {
                        echo '<tr>';
                        echo '<td>'.rewriteType($test['analyseType']).'</td>';
                        echo '<td colspan="4" style="width: 60%">';
                        $body=json_decode($test['body'],true);
                        if(isset($body['try'])){
                            $cntTry = count($body)==1? 0:count($body['try']);
                            if($cntTry>0){
                                echo '<table>';
                                for($i=0;$i<$cntTry;$i++){
                                    for($j=0;$j<count($body['try'][$i]);$j++){
                                        echo '<tr>';
                                        echo '<td>'.($i+1).'.te'.'</td>';

                                        if(strpos($test['analyseType'],'misch')){
                                            $studentInput=$body['try'][$i][$j]['student'];
                                            $mixerInput=$body['try'][$i][$j]['mixer'];
                                            if(!empty($studentInput)){
                                                echo '<td>'.$studentInput['kationen'].' </br>'.$studentInput['anionen'].'</td>';
                                                echo '<td>'.$mixerInput['kationen'].' </br>'.$mixerInput['anionen'].'</td>';
                                            }else{
                                                echo '<td>nicht eingegeben</td>';
                                                echo '<td>'.$mixerInput['kationen'].' </br>'.$mixerInput['anionen'].'</td>';
                                            }
                                            if($body['try'][$i][$j]['correct']=='true'){
                                                echo '<td> √ </td>';
                                            }else{
                                                echo '<td> × </td>';
                                            }
                                        }else{ //einzel analyse
                                            echo '<td>'.$body['try'][$i][$j]['student'].'</td>';
                                            echo '<td>'.$body['try'][$i][$j]['mixer'].'</td>';
                                            if($body['try'][$i][$j]['correct']=='true'){
                                                echo '<td> √ </td>';
                                            }else{
                                                echo '<td> × </td>';
                                            }
                                        }
                                        echo '</tr>';
                                    }
                                }
                                echo '</table>';
                            }
                        }
                        echo '</td>';
                        if(is_null($test['punkt'])){   //TODO here
                            echo '<td>Not finished</td>';
                        }else
                            echo '<td>'.$test['punkt'].'</td>';
                        echo '</tr>';
                    }
                    echo '</table>';
                }
                echo '</td>';
                echo '</tr>';
            }?>
        </table>
    <div>

<script>

    $(document).ready(function(){
    var html= $('div.stdTable').html();

    //add more symbols if needed...
    while (html.indexOf('√') != -1) html = html.replace('√', '&radic;');
    while (html.indexOf('×') != -1) html = html.replace('×', '&times;');

    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
    });

</script>


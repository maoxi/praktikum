<?php
echo $this->element('student_action',array('cnt_kurses'=>$this->Session->read('Auth.cnt_kurses')));
?>

<?php
/**
 * @var $this view
 */
?>
<div class="students form">
    <h2><?php echo 'Punkteübersicht'; ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>

        <tr>
            <th><?php echo $this->Paginator->sort('saal_id','Saal'); ?></th>
            <th><?php echo $this->Paginator->sort('kurse_id','Letzte Eingabe');?></th>
            <th><?php echo $this->Paginator->sort('note','Note'); ?></th>
            <th><?php echo 'Analyse'; ?></th>
            <th><?php echo 'Punkte'; ?></th>
           <!-- <th><?php /*echo $this->Paginator->sort('punkt');*/?></th>-->
        </tr>
        </thead>
        <tbody>
        <?php foreach ($belegungs as $belegung): ?>
            <tr>
                <td><?php echo h($belegung['Saal']['saal_name']); ?>&nbsp;</td>
                <td><?php echo h($belegung['Kurse']['created_date'])?></td>
                <td><?php echo h($belegung['Belegung']['note']); ?>&nbsp;</td>
                <td colspan="2">
                    <table>
                    <?php foreach($belegung['Test'] as $eachTest): ?>
                        <tr><td><?php echo rewriteType($eachTest['analyseType']); ?>&nbsp;</td>
                        <td><?php
                            echo $eachTest['punkt'];
                            ?>&nbsp;</td></tr>
                    <?php endforeach; ?>
                    <tr><td><b>Gesamtpunktzahl (max.160)</b></td>
                        <td>
                            <?php if(is_null($belegung['Belegung']['punkt'])){
                                echo 'Not finished';
                            }else
                                echo $belegung['Belegung']['punkt'];
                            ?> &nbsp;
                        </td>
                    </tr>
                    </table>
                </td>


            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>



<div class="assistents index">
	<h2><?php echo __('Assistenten'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('first_name'); ?></th>
            <th><?php echo $this->Paginator->sort('last_name'); ?></th>
			<th><?php echo $this->Paginator->sort('pwd'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($assistents as $assistent): ?>
	<tr>
		<td><?php echo h($assistent['Assistent']['id']); ?>&nbsp;</td>
		<td><?php echo h($assistent['Assistent']['first_name']); ?>&nbsp;</td>
        <td><?php echo h($assistent['Assistent']['last_name']); ?>&nbsp;</td>
		<td><?php echo h($assistent['Assistent']['pwd']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $assistent['Assistent']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $assistent['Assistent']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $assistent['Assistent']['id']), array(), __('Are you sure you want to delete # %s?', $assistent['Assistent']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Assistent'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Belegungs'), array('controller' => 'belegungs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Belegung'), array('controller' => 'belegungs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Saals'), array('controller' => 'saals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saal'), array('controller' => 'saals', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="assistents form">
<?php echo $this->Form->create('Assistent'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Assistent'); ?></legend>
	<?php
		echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
		echo $this->Form->input('pwd');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Assistents'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Belegungs'), array('controller' => 'belegungs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Belegung'), array('controller' => 'belegungs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Saals'), array('controller' => 'saals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saal'), array('controller' => 'saals', 'action' => 'add')); ?> </li>
	</ul>
</div>

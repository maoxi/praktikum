<?php
/**
 * @var $this view
 */
?>

<?php
echo $this->element('assistent_action');

echo '<div class="level2">';
echo '<h2>Saal Belegung</h2>';
?>

<table cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th><?php echo $this->Paginator->sort('saal_id'); ?></th>
        <th><?php echo $this->Paginator->sort('mixer_id'); ?></th>
        <th><?php echo $this->Paginator->sort('student_id'); ?></th>
        <th class="actions"><?php echo __('Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($belegungs as $belegung): ?>
        <tr>
            <td><?php echo h($belegung['Saal']['saal_name']); ?>&nbsp;</td>
            <td><?php echo h($belegung['Saal']['Mixer']['first_name'].' '.$belegung['Saal']['Mixer']['last_name']); ?>&nbsp;</td>
            <td><?php echo h($belegung['Student']['first_name'].' '.$belegung['Student']['last_name']); ?>&nbsp;</td>
            <td class="actions">
                <a class="showlevel3" id='belegung_<?php echo $belegung['Belegung']['id']; ?>'>Verlauf</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>

<?php foreach ($belegungs as $belegung): ?>
    <div class="level3" id="level3belegung_<?php echo $belegung['Belegung']['id']; ?>" style="display:none">
        <h2><?php echo __('Verlauf'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th><?php echo 'AnalyseType'; ?></th>
                <th><?php echo 'Versuch'; ?></th>
                <th><?php echo 'Mixer eingabe'; ?></th>
                <th><?php echo 'Student eingabe'; ?></th>
                <th><?php echo 'Korrekt'; ?></th>
                <th><?php echo 'Punkt'; ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($belegung['Test'] as $test) {
                echo '<tr>';
                echo '<td>' . rewriteType($test['analyseType']) . '</td>';
                echo '<td colspan="4">';
                $body = json_decode($test['body'], true);
                if (isset($body['try'])) {
                    $cntTry = count($body) == 1 ? 0 : count($body['try']);
                    if ($cntTry > 0) {
                        echo '<table>';
                        for ($i = 0; $i < $cntTry; $i++) {
                            for ($j = 0; $j < count($body['try'][$i]); $j++) {
                                echo '<tr>';
                                echo '<td>' . ($i + 1) . '.te' . '</td>';
                                if (strpos($test['analyseType'], 'misch')) {
                                    $mixerInput = $body['try'][$i][$j]['mixer'];
                                    $studentInput = $body['try'][$i][$j]['student'];
                                    if (!empty($studentInput)) {
                                        echo '<td>' . $mixerInput['kationen'] . ' </br>' . $mixerInput['anionen'] . '&nbsp;</td>';
                                        echo '<td>' . $studentInput['kationen'] . ' </br>' . $studentInput['anionen'] . '&nbsp;</td>';
                                    } else {
                                        echo '<td>' . $mixerInput['kationen'] . ' </br>' . $mixerInput['anionen'] . '&nbsp;</td>';
                                        echo '<td> bearbeiten gerade &nbsp;</td>';
                                    }
                                } else { //einzel analyse
                                    echo '<td>' . $body['try'][$i][$j]['mixer'] . '&nbsp;</td>';
                                    echo '<td>' . $body['try'][$i][$j]['student'] . '&nbsp;</td>';
                                }
                                if (strpos(json_encode($body['try'][$i][$j]['correct']), 'true')) {
                                    echo '<td> √ </td>';
                                } elseif (!empty($body['try'][$i][$j]['correct'])) {
                                    echo '<td> × </td>';
                                }
                                echo '</tr>';
                            }
                        }
                        echo '</table>';
                    }
                }
                echo '</td>';
                if (is_null($test['punkt'])) {
                    echo '<td>Not finished</td>';
                } else{
                    echo '<td>' . $test['punkt'] . '</td>';

                }
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
<?php endforeach; ?>

<script>

    $(document).ready(function () {
        $('div.level2').click(level2);
    });

</script>
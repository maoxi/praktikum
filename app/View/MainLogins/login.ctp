<?php
/**
 * @var $this view
 */
?>

    <div class="mainLogins">
        <h2>AC I Praktikum </h2>
        <?php
        echo $this->Form->create('MainLogin', array('action' => 'login'));
        echo $this->Form->input('cipKennung',array('label'=>'CIPKennung'));
        echo $this->Form->input('pwd',array('label'=>'Kennwort','type'=>'password'));
        echo $this->Form->input('role',array('options'=>array('student','mixer','admin','assistent')));
        echo $this->Form->end('Login');
        ?>
    </div>



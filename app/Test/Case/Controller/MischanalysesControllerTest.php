<?php
App::uses('MischanalysesController', 'Controller');

/**
 * MischanalysesController Test Case
 *
 */
class MischanalysesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mischanalysis'
	);

}

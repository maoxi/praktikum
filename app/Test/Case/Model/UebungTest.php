<?php
App::uses('Uebung', 'Model');

/**
 * Uebung Test Case
 *
 */
class UebungTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.uebung'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Uebung = ClassRegistry::init('Uebung');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Uebung);

		parent::tearDown();
	}

}

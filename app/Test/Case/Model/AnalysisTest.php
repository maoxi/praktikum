<?php
App::uses('Analysis', 'Model');

/**
 * Analysis Test Case
 *
 */
class AnalysisTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.analysis'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Analysis = ClassRegistry::init('Analysis');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Analysis);

		parent::tearDown();
	}

}

<?php
App::uses('Mixer', 'Model');

/**
 * Mixer Test Case
 *
 */
class MixerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mixer',
		'app.belegung',
		'app.student',
		'app.uebung'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Mixer = ClassRegistry::init('Mixer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Mixer);

		parent::tearDown();
	}

}

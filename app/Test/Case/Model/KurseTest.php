<?php
App::uses('Kurse', 'Model');

/**
 * Kurse Test Case
 *
 */
class KurseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.kurse'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Kurse = ClassRegistry::init('Kurse');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Kurse);

		parent::tearDown();
	}

}

<?php
App::uses('Einzelanaly', 'Model');

/**
 * Einzelanaly Test Case
 *
 */
class IonTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.einzelanaly'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Einzelanaly = ClassRegistry::init('Einzelanaly');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Einzelanaly);

		parent::tearDown();
	}

}

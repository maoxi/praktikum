<?php
App::uses('Assistent', 'Model');

/**
 * Assistent Test Case
 *
 */
class AssistentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.assistent',
		'app.belegung',
		'app.student',
		'app.uebung',
		'app.saal',
		'app.mixer',
		'app.test'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Assistent = ClassRegistry::init('Assistent');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Assistent);

		parent::tearDown();
	}

}

<?php
App::uses('Saal', 'Model');

/**
 * Saal Test Case
 *
 */
class SaalTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.saal',
		'app.mixer',
		'app.belegung',
		'app.student',
		'app.uebung',
		'app.test'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Saal = ClassRegistry::init('Saal');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Saal);

		parent::tearDown();
	}

}

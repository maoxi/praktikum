<?php
App::uses('Mischanalysis', 'Model');

/**
 * Mischanalysis Test Case
 *
 */
class MischanalysisTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mischanalysis'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Mischanalysis = ClassRegistry::init('Mischanalysis');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Mischanalysis);

		parent::tearDown();
	}

}

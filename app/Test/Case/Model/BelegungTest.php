<?php
App::uses('Belegung', 'Model');

/**
 * Belegung Test Case
 *
 */
class BelegungTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.belegung'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Belegung = ClassRegistry::init('Belegung');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Belegung);

		parent::tearDown();
	}

}
